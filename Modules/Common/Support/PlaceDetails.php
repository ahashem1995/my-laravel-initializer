<?php


namespace Modules\Common\Support;


use Illuminate\Support\Arr;

class PlaceDetails
{
    /**
     * @var string $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $formattedAddress
     */
    private $formattedAddress;

    /**
     * @var array $addressComponents
     */
    private $addressComponents;

    /**
     * @var string $vicinity
     */
    private $vicinity;

    /**
     * @var float $lat
     */
    private $lat;

    /**
     * @var float $lng
     */
    private $lng;

    /**
     * @var array $types
     */
    private $types;

    /**
     * @var int $utcOffset
     *
     * UTC offset in minutes.
     */
    private $utcOffset;


    /**
     * PlaceDetails constructor.
     * @param object $placeDetailsResponse
     */
    public function __construct(object $placeDetailsResponse)
    {
        $result = $placeDetailsResponse->result;

        $this->id = $result->place_id;
        $this->name = $result->name;
        $this->formattedAddress = $result->formatted_address;
        $this->addressComponents = $result->address_components;
        $this->vicinity = $result->vicinity;
        $this->lat = $result->geometry->location->lat;
        $this->lng = $result->geometry->location->lng;
        $this->types = $result->types;
        $this->utcOffset = $result->utc_offset;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getFormattedAddress(): string
    {
        return $this->formattedAddress;
    }

    /**
     * @return string
     */
    public function getVicinity(): string
    {
        return $this->vicinity;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @return int
     */
    public function getUtcOffset(): int
    {
        return $this->utcOffset;
    }

    /**
     * @return string|null
     */
    public function getCityName(): ?string
    {
        foreach ($this->addressComponents as $addressComponent)
        {
           $type = Arr::first($addressComponent->types, function ($value, $key) {
                return $value === 'locality';
           });

           if ($type) return $addressComponent->long_name;
        }
        return null;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        foreach ($this->addressComponents as $addressComponent)
        {
            $type = Arr::first($addressComponent->types, function ($value, $key) {
                return $value === 'country';
            });

            if ($type) return $addressComponent->short_name;
        }
        return null;
    }

}
