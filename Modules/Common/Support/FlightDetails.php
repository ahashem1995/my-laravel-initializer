<?php


namespace Modules\Common\Support;


use App\Utils\DateUtil;
use Carbon\Carbon;

class FlightDetails
{
    /**
     * @var array $schedule
     */
    private $schedule;

    /**
     * @var array $appendix;
     */
    private $appendix;

    /**
     * FlightDetails constructor.
     * @param object $flightStatsResponse
     */
    public function __construct(object $flightStatsResponse)
    {
        $this->schedule = $flightStatsResponse->scheduledFlights;
        $this->appendix = $flightStatsResponse->appendix;
    }

    /**
     * @return array
     */
    public function getSchedule(): array
    {
        $schedule = [];
        foreach ($this->schedule as $scheduleItem)
        {
            $carrierIcao = null;
            $carrierName = null;

            $departureAirportIcao = null;
            $departureAirportTimezone = null;

            $arrivalAirportIcao = null;
            $arrivalAirportTimezone = null;

            foreach ($this->appendix->airlines as $appendixItem)
            {
                if ($appendixItem->fs === $scheduleItem->carrierFsCode)
                {
                    $carrierIcao = $appendixItem->icao;
                    $carrierName = $appendixItem->name;
                }
            }

            foreach ($this->appendix->airports as $appendixItem)
            {
                if ($appendixItem->fs === $scheduleItem->departureAirportFsCode)
                {
                    $departureAirportIcao = $appendixItem->icao;
                    $departureAirportTimezone = $appendixItem->timeZoneRegionName;
                    continue;
                }
                if ($appendixItem->fs === $scheduleItem->arrivalAirportFsCode)
                {
                    $arrivalAirportIcao = $appendixItem->icao;
                    $arrivalAirportTimezone = $appendixItem->timeZoneRegionName;
                    continue;
                }
            }

            $schedule[] = [
                'carrierName' => $carrierName,
                'carrierIcao' => $carrierIcao,
                'flightNumber' => $scheduleItem->flightNumber,
                'departureAirportIcao' => $departureAirportIcao,
                'departureTime' => Carbon::createFromFormat(DateUtil::ISO8601, $scheduleItem->departureTime, $departureAirportTimezone)->setTimezone('UTC'),
                'arrivalAirportIcao' => $arrivalAirportIcao,
                'arrivalTime' => Carbon::createFromFormat(DateUtil::ISO8601, $scheduleItem->arrivalTime, $arrivalAirportTimezone)->setTimezone('UTC')
            ];
        }

        return $schedule;
    }

}
