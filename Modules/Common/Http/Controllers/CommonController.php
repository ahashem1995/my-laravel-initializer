<?php

namespace Modules\Common\Http\Controllers;

use App\Http\Controllers\MyVehicleBaseController;
use App\Utils\CollectionUtil;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Elasticsearch\ClientBuilder;
use Modules\Common\Entities\Resolvers\SearchableDocumentIndexResolver;
use Modules\Common\Http\Requests\SearchAllRequest;

class CommonController extends MyVehicleBaseController
{
    /**
     * Display a listing of the resource.
     * @param SearchAllRequest $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function search(SearchAllRequest $request)
    {
        $client = ClientBuilder::create()->setHosts(config('scout_elastic.client.hosts'))->build();

        $params = [
            'index' => join(',', array_keys(SearchableDocumentIndexResolver::INDEX_CONFIGURATIONS)),
            'body' => [
                'query' => [
                    "query_string" => [
                        "query" => $request['query']
                    ]
                ]
            ]
        ];

        $response = $client->search($params);
        $hits = CollectionUtil::get($response['hits']['hits'], null);
        $returnValue = [];

        if (!empty($hits)) {
            foreach ($hits as $hit) {
                $documentType = $hit['_type'];
                $entry = SearchableDocumentIndexResolver::formatDocument($documentType, $hit['_source']);
                $entry['type'] = $documentType;

                $returnValue[] = $entry;
            }
        }

        return $this->ok($returnValue);
    }
}
