<?php

namespace Modules\Common\Http\Controllers;

use App\Http\Controllers\MyVehicleBaseController;
use App\Transformers\PaginationResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Modules\Common\Entities\Attachment;
use Modules\Common\Entities\Resolvers\AttachmentsPartitionResolver;
use Modules\Common\Entities\Resolvers\NotesPartitionResolver;
use Modules\Common\Http\Requests\SaveAttachmentRequest;
use Modules\Common\Services\AttachmentService;
use Modules\Common\Transformers\AttachmentResource;

class AttachmentController extends MyVehicleBaseController
{
    /**
     * @var AttachmentService
     */
    private $service;

    public function __construct(AttachmentService $service)
    {
        $this->service = $service;
    }

    /*
     * Display a listing of the resource.
     * @param Request $request
     * @return PaginationResource
     */
    public function index(Request $request, $ownerId)
    {
        $className = $this->resolveOwnerName($request);

        $owner = new $className();
        $owner->id = $ownerId;

        $notes = $this->service->paginateByOwnerWith($owner, ['uploadedBy' => []]);

        return new PaginationResource(AttachmentResource::class, $notes);


    }

    /**
     * Show the specified resource.
     * @param $attachmentId
     * @return JsonResponse
     */
    public function show($attachmentId)
    {
        $note = $this->service->find($attachmentId);

        if ($note === null)
            return $this->notFound();

        return $this->ok(new AttachmentResource($note));
    }

    /**
     * Store a newly created resource in storage.
     * @param SaveAttachmentRequest $request
     * @param $ownerId
     * @return JsonResponse
     */
    public function store(SaveAttachmentRequest $request, $ownerId)
    {
        try {
            $className = $this->resolveOwnerName($request);

            $owner = new $className();
            $owner->id = $ownerId;

            $attachment = $this->service->addAttachment($owner, $request->file('file'), auth()->user());
            return $this->ok(new AttachmentResource($attachment));
        } catch (\Exception $exception) {
            report($exception);
            return $this->internalError();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param Attachment $attachment
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function destroy(Attachment $attachment)
    {
        $this->authorize('delete', $attachment);

        if ($this->service->softDeleteObject($attachment))
            return $this->deleted();
        else
            return $this->notFound();
    }


    /**
     * @param Request $request
     * @return string
     */
    private function resolveOwnerName(Request $request): string
    {
        $plural = explode('.', $request->route()->getName())[0];
        return AttachmentsPartitionResolver::MODELS_MODULES[Str::singular($plural)];
    }
}
