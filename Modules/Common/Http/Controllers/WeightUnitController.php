<?php

namespace Modules\Common\Http\Controllers;

use App\Http\Controllers\MyVehicleBaseController;
use Modules\Common\Entities\WeightUnit;

class WeightUnitController extends MyVehicleBaseController
{
    public function __invoke()
    {
        $data = [];
        foreach (WeightUnit::DISPLAY_VALUES as $key => $value)
            $data[] = ['code' => $key, 'display' => $value];

        $data = collect($data);

        return $this->ok($data);
    }
}
