<?php

namespace Modules\Common\Http\Controllers;

use App\Http\Controllers\MyVehicleBaseController;
use App\Transformers\PaginationResource;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Modules\Common\Entities\Note;
use Modules\Common\Entities\NoteDefinition;
use Modules\Common\Entities\Resolvers\NotesPartitionResolver;
use Modules\Common\Http\Requests\SaveNoteRequest;
use Modules\Common\Services\NoteService;
use Modules\Common\Transformers\NoteResource;

class NoteController extends MyVehicleBaseController
{
    /**
     * @var NoteService
    */
    private $service;

    public function __construct(NoteService $noteService)
    {
        $this->service = $noteService;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param $ownerId
     * @return PaginationResource
     * @throws AuthorizationException
     */
    public function index(Request $request, $ownerId)
    {
        $this->authorize('viewAny', Note::class);

        $className = $this->resolveOwnerName($request);

        $owner = new $className();
        $owner->id = $ownerId;

        $notes = $this->service->paginateByOwnerWith($owner, ['createdBy' => [], 'updatedBy' => []]);

        return new PaginationResource(NoteResource::class, $notes);
    }

    /**
     * Store a newly created resource in storage.
     * @param SaveNoteRequest $request
     * @param $ownerId
     * @return JsonResponse
     */
    public function store(SaveNoteRequest $request, $ownerId)
    {
        $className = $this->resolveOwnerName($request);

        $owner = new $className();
        $owner->id = $ownerId;

        $persistedNote = $this->service->addNote($owner, $request->get('content'), auth()->user());

        return $this->ok(new NoteResource($persistedNote));
    }

    /**
     * Show the specified resource.
     * @param Note $note
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function show(Note $note)
    {
        $this->authorize('view', $note);

        return $this->ok(new NoteResource($note));
    }

    /**
     * Update the specified resource in storage.
     * @param SaveNoteRequest $request
     * @param Note $note
     * @return JsonResponse
     */
    public function update(SaveNoteRequest $request, Note $note)
    {
        $persistedNote = $this->service->editNote($note, $request->get('content'), auth()->user());

        return $this->ok(new NoteResource($persistedNote));
    }

    /**
     * Remove the specified resource from storage.
     * @param Note $note
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws \Exception
     */
    public function destroy(Note $note)
    {
        $this->authorize('delete', $note);

        $this->service->softDeleteObject($note);

        return $this->deleted();
    }

    /**
     * @param Request $request
     * @return string
     */
    private function resolveOwnerName(Request $request): string
    {
        $plural = explode('.', $request->route()->getName())[0];
        return NotesPartitionResolver::MODELS_MODULES[Str::singular($plural)];
    }
}
