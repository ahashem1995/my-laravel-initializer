<?php

namespace Modules\Common\Http\Requests;

use App\Http\Requests\BaseFormRequest;

class SearchAllRequest extends BaseFormRequest
{
    protected $queryParametersToValidate = ['query'];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query' => 'required|min:1'
        ];
    }

}

