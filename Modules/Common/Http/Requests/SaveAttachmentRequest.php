<?php

namespace Modules\Common\Http\Requests;

use App\Http\Requests\BaseFormRequest;

class SaveAttachmentRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|file|mimes:jpeg,png,jpg,gif,svg,doc,docx,xls,xlsx,pdf|max:10240' //10 MB
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
