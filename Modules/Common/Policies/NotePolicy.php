<?php

namespace Modules\Common\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Common\Entities\Note;
use Modules\Common\Entities\NotePermissions;
use Spatie\Permission\Models\Role;

class NotePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo(NotePermissions::LIST);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo(NotePermissions::ADD);
    }

    /**
     * @param User $user
     * @param Note $note
     * @return bool
     */
    public function view(User $user, Note $note)
    {
        return $user->hasPermissionTo(NotePermissions::GET);
    }

    /**
     * @param User $user
     * @param Note $note
     * @return bool
     */
    public function update(User $user, Note $note)
    {
        return $user->hasPermissionTo(NotePermissions::EDIT);
    }

    /**
     * @param User $user
     * @param Note $note
     * @return bool
     */
    public function delete(User $user, Note $note)
    {
        return $user->hasPermissionTo(NotePermissions::DELETE);
    }

    /**
     * @param User $user
     * @param Note $note
     * @return bool
     */
    public function restore(User $user, Note $note)
    {
        return $user->hasPermissionTo(NotePermissions::RESTORE);
    }
}
