<?php

namespace Modules\Common\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\User\Repositories\RoleRepository;
use Modules\User\Repositories\UserRepository;
use Spatie\Permission\Models\Role;

class CommonDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param UserRepository $userRepository
     * @param RoleRepository $roleRepository
     * @return void
     * @throws \Exception
     */
    public function run(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        Model::unguard();

        $role = $roleRepository->create(['name' => 'Super Admin']);

        $user = $userRepository->create([
            'name' => 'admin',
            'email' => 'admin@myvehicle.com',
            'password' => '123123',
        ]);

        $user->assignRole($role);

        $user = $userRepository->create([
            'name' => 'Abdulrahman',
            'email' => 'abd@myvehile.com',
            'password' => '123123',
        ]);

        $user->assignRole($role);
    }
}
