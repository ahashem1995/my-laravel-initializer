<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Common\Entities\AttachmentDefinition;
use Modules\Common\Entities\Resolvers\AttachmentsPartitionResolver;

class AlterAttachmentsPartitions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = 'ALTER table ' . AttachmentDefinition::TABLE_NAME . ' PARTITION BY LIST(' . AttachmentDefinition::ATTACHABLE_PARTITION_KEY . ') (';
        $i = 0;
        foreach (AttachmentsPartitionResolver::PARTITIONED_MODELS as $className)
        {
            $query .= 'PARTITION ' . $className::getPlural() . ' VALUES IN (' . AttachmentsPartitionResolver::getModelPartitionKey($className) . ')';
            if ($i !== count(AttachmentsPartitionResolver::PARTITIONED_MODELS) - 1)
                $query .= ',';
            $i++;
        }
        $query .= ');';

//        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
