<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Common\Entities\AttachmentDefinition;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AttachmentDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->unsignedInteger(AttachmentDefinition::ID);
            $table->string(AttachmentDefinition::ATTACHABLE_TYPE);
            $table->unsignedInteger(AttachmentDefinition::ATTACHABLE_ID);
            $table->unsignedInteger(AttachmentDefinition::ATTACHABLE_PARTITION_KEY);
            $table->string(AttachmentDefinition::ORIGINAL_NAME, 255);
            $table->string(AttachmentDefinition::NAME, 255);
            $table->string(AttachmentDefinition::URL, 500);
            $table->unsignedBigInteger(AttachmentDefinition::UPLOADED_BY);

            $table->softDeletes();
            $table->timestamps();
        });

        DB::statement('ALTER TABLE ' . AttachmentDefinition::TABLE_NAME . ' ADD PRIMARY KEY (' . AttachmentDefinition::ID . ', ' . AttachmentDefinition::ATTACHABLE_PARTITION_KEY . ');');
        DB::statement('ALTER TABLE ' . AttachmentDefinition::TABLE_NAME . ' MODIFY COLUMN ' . AttachmentDefinition::ID . ' INT AUTO_INCREMENT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(AttachmentDefinition::TABLE_NAME);
    }
}
