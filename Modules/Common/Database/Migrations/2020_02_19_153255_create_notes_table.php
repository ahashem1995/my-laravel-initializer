<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Common\Entities\NoteDefinition;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(NoteDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->unsignedInteger(NoteDefinition::ID);
            $table->string(NoteDefinition::NOTABLE_TYPE);
            $table->unsignedInteger(NoteDefinition::NOTABLE_ID);
            $table->unsignedInteger(NoteDefinition::NOTABLE_PARTITION_KEY);
            $table->text(NoteDefinition::CONTENT);
            $table->unsignedBigInteger(NoteDefinition::CREATED_BY);
            $table->unsignedBigInteger(NoteDefinition::UPDATED_BY);
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement('ALTER TABLE ' . NoteDefinition::TABLE_NAME . ' ADD PRIMARY KEY (' . NoteDefinition::ID . ', ' . NoteDefinition::NOTABLE_PARTITION_KEY . ');');
        DB::statement('ALTER TABLE ' . NoteDefinition::TABLE_NAME . ' MODIFY COLUMN ' . NoteDefinition::ID . ' INT AUTO_INCREMENT;');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(NoteDefinition::TABLE_NAME);
    }
}
