<?php

namespace Modules\Common\Services;

use App\Entities\BaseModel;
use App\Services\BaseService;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Common\Entities\Note;
use Modules\Common\Entities\NoteDefinition;
use Modules\Common\Entities\Resolvers\NotesPartitionResolver;
use Modules\Common\Repositories\NoteRepository;

class NoteService extends BaseService
{
	public function __construct(NoteRepository $repository) {
		parent::__construct($repository);
	}

    /**
     * @param $owner
     * @param array $tables
     * @param int $perPage
     * @param array $order
     * @param array $conditions
     * @param bool $onlyTrashed
     * @param array $columns
     * @return mixed|null
     */
    public function paginateByOwnerWith($owner, $tables = array(), $perPage = 15, $order = array(), $conditions = array(), $onlyTrashed = false, $columns = array('*'))
    {
        $conditions[] = [NoteDefinition::NOTABLE_PARTITION_KEY, '=', NotesPartitionResolver::getModelPartitionKey($owner::getClass())];
        $conditions[] = [NoteDefinition::NOTABLE_ID, '=', $owner->id];

        return parent::paginateWith($tables, $perPage, $order, $conditions, $onlyTrashed, $columns);
    }

    /**
     * @param $owner
     * @param string $content
     * @param User $createdBy
     * @return Model
     */
    public function addNote($owner, string $content, User $createdBy): Model
    {
        return $this->create([
            NoteDefinition::NOTABLE_ID => $owner->id,
            NoteDefinition::NOTABLE_TYPE => $owner::getClass(),
            NoteDefinition::CONTENT => $content,
            NoteDefinition::CREATED_BY => $createdBy->id,
            NoteDefinition::UPDATED_BY => $createdBy->id
        ]);
    }

    /**
     * @param $note
     * @param string $content
     * @param User $updatedBy
     * @return Note
     */
    public function editNote($note, string $content, User $updatedBy): Note
    {
        if (!($note instanceof Note))
            $note = $this->find($note);

        if ($note === null)
            throw new ModelNotFoundException();

        $note->fill([
            NoteDefinition::CONTENT => $content,
            NoteDefinition::UPDATED_BY => $updatedBy->id
        ]);
        $this->save($note);

        return $note;
    }
}
