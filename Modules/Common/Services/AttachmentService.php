<?php

namespace Modules\Common\Services;

use App\Entities\BaseModel;
use App\Services\BaseService;

use Aws\StorageGateway\Exception\StorageGatewayException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Config;
use Modules\Common\Entities\Attachment;
use Modules\Common\Entities\AttachmentDefinition;
use Modules\Common\Entities\NoteDefinition;
use Modules\Common\Entities\Resolvers\AttachmentsPartitionResolver;
use Modules\Common\Entities\Resolvers\NotesPartitionResolver;
use Modules\Common\Repositories\AttachmentRepository;

class AttachmentService extends BaseService
{
    public function __construct(AttachmentRepository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * @param $owner
     * @param array $tables
     * @param int $perPage
     * @param array $order
     * @param array $conditions
     * @param bool $onlyTrashed
     * @param array $columns
     * @return mixed|null
     */
    public function paginateByOwnerWith($owner, $tables = array(), $perPage = 15, $order = array(), $conditions = array(), $onlyTrashed = false, $columns = array('*'))
    {
        $conditions[] = [AttachmentDefinition::ATTACHABLE_PARTITION_KEY, '=', AttachmentsPartitionResolver::getModelPartitionKey($owner::getClass())];
        $conditions[] = [AttachmentDefinition::ATTACHABLE_ID, '=', $owner->id];

        return parent::paginateWith($tables, $perPage, $order, $conditions, $onlyTrashed, $columns);
    }

    /**
     * @param $owner
     * @param UploadedFile $file
     * @param $uploadedBy
     * @return BaseModel|Attachment
     * @throws \Exception
     */
    public function addAttachment($owner, $file, $uploadedBy): Attachment
    {
        $fileName = time() . '.' . $file->clientExtension();
        $originalFileName = $file->getClientOriginalName();
        $filePath = env('AWS_BUCKET', 'public') . '/' . $fileName;

        if (Storage::put($filePath, file_get_contents($file))) {
            return $this->create([
                AttachmentDefinition::ATTACHABLE_ID => $owner->id,
                AttachmentDefinition::ATTACHABLE_TYPE => $owner::getClass(),
                AttachmentDefinition::ORIGINAL_NAME => $originalFileName,
                AttachmentDefinition::NAME => $fileName,
                AttachmentDefinition::URL => Storage::disk('s3')->url($filePath),
                AttachmentDefinition::UPLOADED_BY => $uploadedBy->id
            ]);
        } else {
            throw new \Exception("Couldn't Upload attachment");
        }
    }

}
