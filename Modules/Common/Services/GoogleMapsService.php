<?php


namespace Modules\Common\Services;


use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use Modules\Common\Support\PlaceDetails;

class GoogleMapsService
{
    /**
     * @var string $apiKey
     *
     * API key for accessing the Google Maps API.
     */
    protected $apiKey;

    /**
     * @var string $mapsBaseUrl
     *
     * Base url for all Google Maps API services.
     */
    protected $mapsBaseUrl = 'https://maps.googleapis.com/maps/api/';

    /**
     * @var string $timezoneEndpoint
     *
     * Endpoint for Google Maps Timezone API service.
     */
    protected $timezoneEndpoint = 'timezone/json';


    /**
     * @var string $placeDetailsEndpoint
     *
     * Endpoint for Google Maps Places Place Details API service.
     */
    protected $placesEndpoint = 'place/details/json';

    /**
     * @var string $placeDetailsFields
     *
     * Fields to fetch from the Google Maps Places Place Details API service.
     */
    protected $placeDetailsFields = 'address_components,formatted_address,geometry,international_phone_number,name,place_id,types,utc_offset,vicinity';

    /**
     * GuzzleHttp client instance.
     *
     * @var Client $client
     */
    protected $client;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;

        $this->client = new Client([
            'base_uri' => $this->mapsBaseUrl,
            'timeout' => 60
        ]);
    }

    /**
     * @param float $lat
     * @param float $lng
     * @param int|null $timestamp
     * @return string
     */
    public function getTimezone(float $lat, float $lng, ?int $timestamp = null): string
    {
        try
        {
            $response = $this->client->get($this->timezoneEndpoint, [
               'headers' => [
                   'Accept' => 'application/json'
               ],
               'query' => [
                   'location' => $lat . ',' . $lng,
                   'timestamp' => $timestamp ?? Carbon::now()->getTimestamp(),
                   'key' => $this->apiKey
               ]
            ]);

            return json_decode($response->getBody())->timeZoneId;
        }
        catch (TransferException $exception)
        {
            throw $exception;
        }
    }

    /**
     * @param $placeId
     * @return PlaceDetails
     */
    public function getPlaceDetails($placeId): PlaceDetails
    {
        try
        {
            $response = $this->client->get($this->placesEndpoint, [
                'headers' => [
                    'Accept' => 'application/json'
                ],
                'query' => [
                    'place_id' => $placeId,
                    'fields' => $this->placeDetailsFields,
                    'key' => $this->apiKey
                ]
            ]);


            return new PlaceDetails(json_decode($response->getBody()));
        }
        catch (TransferException $exception)
        {
            throw $exception;
        }
    }

}
