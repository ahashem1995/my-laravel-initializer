<?php


namespace Modules\Common\Services;


use Carbon\Carbon;
use Carbon\CarbonInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use Modules\Common\Support\FlightDetails;

class FlightStatsService
{
    /**
     * @var string $appId
     *
     * App ID for accessing the FlightStats API.
     */
    protected $appId;

    /**
     * @var string $appKey
     *
     * App secret.
     */
    protected $appKey;

    /**
     * @var string $fsBaseUrl
     *
     * Base url for all FlightStats API services.
     */
    protected $fsBaseUrl = 'https://api.flightstats.com/flex/';

    /**
     * @var string $schedulesEndpoint
     *
     * Endpoint for FlightStats Schedules API.
     */
    protected $schedulesEndpoint = 'schedules/rest/v1/json/flight';

    /**
     * @var Client $client
     *
     * GuzzleHttp client instance.
     */
    protected $client;

    public function __construct(string $appId, string $appKey)
    {
        $this->appId = $appId;
        $this->appKey = $appKey;

        $this->client = new Client([
            'base_uri' => $this->fsBaseUrl,
            'timeout' => 60
        ]);
    }

    /**
     * @param string $carrierIcao
     * @param string $flightNumber
     * @param Carbon $date
     * @return FlightDetails
     */
    public function getFlightSchedule(string $carrierIcao, string $flightNumber, Carbon $date)
    {
        try
        {
            $year = $date->year;
            $month = $date->month;
            $day = $date->day;

            $response = $this->client->get("$this->schedulesEndpoint/$carrierIcao/$flightNumber/departing/$year/$month/$day", [
                'headers' => [
                    'Accept' => 'application/json'
                ],
                'query' => [
                    'appId' => $this->appId,
                    'appKey' => $this->appKey,
                    'codeType' => 'ICAO'
                ]
            ]);
        }
        catch (TransferException $exception)
        {
            throw $exception;
        }

        return new FlightDetails(json_decode($response->getBody()));
    }


}
