<?php


namespace Modules\Common\Entities;


use App\Entities\BaseFields;

class NoteDefinition
{
    const TABLE_NAME = 'notes';
    const ID = BaseFields::ID;
    const NOTABLE_TYPE = 'notable_type';
    const NOTABLE_ID = 'notable_id';
    const NOTABLE_PARTITION_KEY = 'notable_partition_key';
    const CONTENT = 'content';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';

    const FILLABLES = [
        self::NOTABLE_TYPE,
        self::NOTABLE_ID,
        self::CONTENT,
        self::CREATED_BY,
        self::UPDATED_BY
    ];

}
