<?php


namespace Modules\Common\Entities;


use App\Entities\BaseFields;

class ContactInfoDefinition
{
    const ID = BaseFields::ID;
    const PHONE_NUMBER = 'phone_number';

    const FILLABLES = [
        self::PHONE_NUMBER
    ];
}
