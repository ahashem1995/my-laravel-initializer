<?php


namespace Modules\Common\Entities;


use App\Entities\BasePermissions;

class NotePermissions extends BasePermissions
{
    const LIST = 'list notes';
    const ADD = 'add note';
    const GET = 'get note';
    const EDIT = 'edit note';
    const DELETE = 'delete note';
    const RESTORE = 'restore note';

    /**
     * @inheritDoc
     */
    public static function toArray(): array
    {
        return [
            self::LIST,
            self::ADD,
            self::GET,
            self::EDIT,
            self::DELETE,
            self::RESTORE
        ];
    }
}
