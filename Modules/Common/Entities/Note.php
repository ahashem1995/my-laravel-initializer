<?php

namespace Modules\Common\Entities;

use App\Entities\BaseModel;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Common\Entities\Resolvers\NotesPartitionResolver;

class Note extends BaseModel
{
    use SoftDeletes;

    protected $table = NoteDefinition::TABLE_NAME;
    protected $fillable = NoteDefinition::FILLABLES;

    public function createdBy()
    {
        return $this->belongsTo(User::class, NoteDefinition::CREATED_BY);
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, NoteDefinition::UPDATED_BY);
    }

    public function setNotableTypeAttribute($value) {
        $this->attributes[NoteDefinition::NOTABLE_TYPE] = $value;
        $this->attributes[NoteDefinition::NOTABLE_PARTITION_KEY] = NotesPartitionResolver::getModelPartitionKey($value);
    }

}
