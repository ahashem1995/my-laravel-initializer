<?php


namespace Modules\Common\Entities\Traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\Common\Entities\Note;
use Modules\Common\Entities\NoteDefinition;
use Modules\Common\Entities\Resolvers\NotesPartitionResolver;

trait Notable
{
    public function notes()
    {
        return $this->morphMany(Note::class, 'notable');
    }
}

class NotedMorphManyPartitioned extends MorphMany
{
    public function addConstraints()
    {
        if (static::$constraints) {
            parent::addConstraints();

            $this->query->where(NoteDefinition::NOTABLE_PARTITION_KEY, NotesPartitionResolver::getModelPartitionKey($this->morphClass));
        }
    }
}

