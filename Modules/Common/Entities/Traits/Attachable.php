<?php


namespace Modules\Common\Entities\Traits;


use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\Common\Entities\Attachment;
use Modules\Common\Entities\AttachmentDefinition;
use Modules\Common\Entities\Resolvers\AttachmentsPartitionResolver;

trait Attachable
{
    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }
}

class AttachedMorphManyPartitioned extends MorphMany
{
    public function addConstraints()
    {
        if (static::$constraints) {
            parent::addConstraints();

            $this->query->where(AttachmentDefinition::ATTACHABLE_PARTITION_KEY,
                AttachmentsPartitionResolver::getModelPartitionKey($this->morphClass));
        }
    }
}
