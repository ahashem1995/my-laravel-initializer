<?php


namespace Modules\Common\Entities;


use App\Entities\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class ContactInfo extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ContactInfoDefinition::FILLABLES;
}
