<?php


namespace Modules\Common\Entities\Base;


use App\Entities\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\Common\Entities\AttachmentDefinition;
use Modules\Common\Entities\NoteDefinition;
use Modules\Common\Entities\Traits\AttachedMorphManyPartitioned;
use Modules\Common\Entities\Traits\NotedMorphManyPartitioned;

class PartitionedModel extends BaseModel
{
    protected function newMorphMany(Builder $query, Model $parent, $type, $id, $localKey)
    {
        if ($type == 'notes.' . NoteDefinition::NOTABLE_TYPE)
            return new NotedMorphManyPartitioned($query, $parent, $type, $id, $localKey);
        else if ($type == 'attachments.' . AttachmentDefinition::ATTACHABLE_TYPE)
            return new AttachedMorphManyPartitioned($query, $parent, $type, $id, $localKey);
        else
            return new MorphMany($query, $parent, $type, $id, $localKey);
    }
}
