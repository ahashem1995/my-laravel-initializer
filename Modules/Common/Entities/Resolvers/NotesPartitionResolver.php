<?php


namespace Modules\Common\Entities\Resolvers;


use App\Entities\BaseModel;

class NotesPartitionResolver
{

    const PARTITIONED_MODELS = [
//        Airport::class
    ];

    const MODELS_MODULES = [
//        'airport' => 'Modules\\Flight\\Entities\\Airport',
    ];

    /**
     * @param BaseModel|string $model
     * @return mixed
     */
    public static function getModelPartitionKey($model)
    {
        if ($model instanceof BaseModel)
            $model = $model::getClass();

        return crc32($model);
    }

}
