<?php


namespace Modules\Common\Entities\Resolvers;


use App\Entities\BaseModel;
use Modules\Company\Entities\Company;
use Modules\Company\Entities\Customer;
use Modules\Company\Entities\Person;
use Modules\Flight\Entities\Aircraft;
use Modules\Flight\Entities\Airport;
use Modules\Operation\Entities\Operation;

class AttachmentsPartitionResolver
{
    const PARTITIONED_MODELS = [
//        Airport::class,
    ];

    const MODELS_MODULES = [
//        'airport' => 'Modules\\Flight\\Entities\\Airport',
    ];

    /**
     * @param BaseModel|string $model
     * @return mixed
     */
    public static function getModelPartitionKey($model)
    {
        if ($model instanceof BaseModel)
            $model = $model::getClass();

        return crc32($model);
    }
}
