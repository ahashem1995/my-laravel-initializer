<?php

namespace Modules\Common\Entities\Resolvers;

use Modules\Company\Entities\Company;
use Modules\Company\Entities\Customer;
use Modules\Flight\Entities\Aircraft;
use Modules\Flight\Entities\Airport;

class SearchableDocumentIndexResolver
{
    const INDEX_CONFIGURATIONS = [
        //'cities' => City::class,
        'airports' => Airport::class,
        'aircraft' => Aircraft::class,
        'companies' => Company::class,
        'customers' => Customer::class,
    ];

    private static function getModel($forType): ?string
    {
        return self::INDEX_CONFIGURATIONS[$forType];
    }

    public static function formatDocument($forType, $document): ?array
    {
        $model = self::getModel($forType);

        if ($model == null)
            return null;

        return $model::formatSearchableDocument($document);
    }
}
