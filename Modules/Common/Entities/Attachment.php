<?php


namespace Modules\Common\Entities;


use App\Entities\BaseModel;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Common\Entities\Resolvers\AttachmentsPartitionResolver;

class Attachment extends BaseModel
{
    use SoftDeletes;

    protected $table = AttachmentDefinition::TABLE_NAME;
    protected $fillable = AttachmentDefinition::FILLABLES;

    public function uploadedBy()
    {
        return $this->belongsTo(User::class, AttachmentDefinition::UPLOADED_BY);
    }

    public function setAttachableTypeAttribute($value)
    {
        $this->attributes[AttachmentDefinition::ATTACHABLE_TYPE] = $value;
        $this->attributes[AttachmentDefinition::ATTACHABLE_PARTITION_KEY] = AttachmentsPartitionResolver::getModelPartitionKey($value);
    }
}
