<?php


namespace Modules\Common\Entities;


class WeightUnit
{
    const TONNE = 'T';
    const KILOGRAM = 'KG';

    const SET = [
        self::TONNE,
        self::KILOGRAM,
    ];

    const DISPLAY_VALUES = [
        self::TONNE => 'Tonne',
        self::KILOGRAM => 'Kilogram'
    ];
}
