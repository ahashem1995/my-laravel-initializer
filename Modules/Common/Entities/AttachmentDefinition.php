<?php


namespace Modules\Common\Entities;


use App\Entities\BaseFields;

class AttachmentDefinition
{
    const TABLE_NAME = "attachments";

    const ID = BaseFields::ID;
    const ORIGINAL_NAME = 'original_name';
    const NAME = 'name';
    const URL = 'url';
    const UPLOADED_BY = 'uploaded_by';
    const ATTACHABLE_TYPE = 'attachable_type';
    const ATTACHABLE_ID = 'attachable_id';
    const ATTACHABLE_PARTITION_KEY = 'attachable_partition_key';

    const FILLABLES = [
        self::ATTACHABLE_TYPE,
        self::ATTACHABLE_ID,
        self::ORIGINAL_NAME,
        self::NAME,
        self::URL,
        self::UPLOADED_BY
    ];
}
