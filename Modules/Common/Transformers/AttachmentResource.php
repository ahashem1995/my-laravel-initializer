<?php

namespace Modules\Common\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Common\Entities\AttachmentDefinition;
use Modules\User\Transformers\UserHeaderResource;

class AttachmentResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this[AttachmentDefinition::ID],
            'name' => $this[AttachmentDefinition::ORIGINAL_NAME],
            'url' => $this[AttachmentDefinition::URL],
            'uploaded_by' => new UserHeaderResource($this->uploadedBy)
        ];
    }
}
