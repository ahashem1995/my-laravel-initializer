<?php

namespace Modules\Common\Transformers;

use App\Entities\BaseFields;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use Modules\Common\Entities\NoteDefinition;
use Modules\User\Transformers\UserHeaderResource;

class NoteResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this[NoteDefinition::ID],
            'content' => $this[NoteDefinition::CONTENT],
            'created_by' => new UserHeaderResource($this->createdBy),
            'created_at' => $this[BaseFields::CREATED_AT],
            'updated_by' => new UserHeaderResource($this->updatedBy),
            'updated_at' => $this[BaseFields::UPDATED_AT]
        ];
    }
}
