<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Modules\Common\Entities\Resolvers\AttachmentsPartitionResolver;
use Modules\Common\Entities\Resolvers\NotesPartitionResolver;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$routePrefix = Config::get('common.modulePrefix');

Route::middleware('auth:api')->prefix($routePrefix)->group(function () {

    Route::apiResource('notes', 'NoteController')->only(['show', 'update', 'destroy']);
    Route::apiResource('attachments', 'AttachmentController')->only(['show', 'destroy']);
    Route::get('search', 'CommonController@search')->name('search');
    Route::get('weightUnits', 'WeightUnitController')->name('weightUnit.index');

});

Route::middleware('auth:api')->group(function () {

    foreach (NotesPartitionResolver::PARTITIONED_MODELS as $className) {
        $routePrefix = $className::getModule();

        Route::middleware('auth:api')->prefix($routePrefix)->group(function () use ($className) {

            Route::apiResource($className::getPlural() . '.notes', 'NoteController')->only(['index', 'store']);

        });
    }

    foreach (AttachmentsPartitionResolver::PARTITIONED_MODELS as $className) {
        $routePrefix = $className::getModule();

        Route::middleware('auth:api')->prefix($routePrefix)->group(function () use ($className) {
            Route::apiResource($className::getPlural() . '.attachments', 'AttachmentController')
                ->only(['index', 'store']);
        });
    }

});

