<?php

namespace Modules\Common\Repositories;

use App\Repositories\BaseRepository;

class AttachmentRepository extends BaseRepository
{
	public function __construct($persistentClass, $defaultOrder = ['id' => 'desc']) {
		parent::__construct($persistentClass, $defaultOrder);
	}
}