<?php


namespace Modules\User\Entities;


use App\Entities\BasePermissions;

class PermissionPermissions extends BasePermissions
{
    const LIST = 'list permissions';

    /**
     * @inheritDoc
     */
    public static function toArray(): array
    {
        return [
            self::LIST
        ];
    }
}
