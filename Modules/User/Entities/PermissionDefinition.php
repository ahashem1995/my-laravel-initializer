<?php


namespace Modules\User\Entities;


use App\Entities\BaseFields;

class PermissionDefinition
{
    const TABLE_NAME = 'permissions';

    const ID = BaseFields::ID;
    const NAME = 'name';
}
