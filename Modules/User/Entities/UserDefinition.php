<?php


namespace Modules\User\Entities;


use App\Entities\BaseFields;

class UserDefinition
{
    const TABLE_NAME = 'users';

    const ID = BaseFields::ID;
    const EMAIL = 'email';
    const NAME = 'name';
    const PASSWORD = 'password';

    const SORTABLES = [
        'email' => self::EMAIL,
        'name' => self::NAME,
        'id' => self::ID
    ];
}
