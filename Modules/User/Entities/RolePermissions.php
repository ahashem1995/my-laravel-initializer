<?php


namespace Modules\User\Entities;


use App\Entities\BasePermissions;

class RolePermissions extends BasePermissions
{
    const LIST = 'list roles';
    const ADD = 'add role';
    const GET = 'get role';
    const EDIT = 'edit role';
    const DELETE = 'delete role';
    const RESTORE = 'restore role';

    /**
     * @inheritDoc
     */
    public static function toArray(): array
    {
        return [
            self::LIST,
            self::ADD,
            self::GET,
            self::EDIT,
            self::DELETE,
            self::RESTORE
        ];
    }
}
