<?php


namespace Modules\User\Entities;


use App\Entities\BasePermissions;

class UserPermissions extends BasePermissions
{
    const LIST = 'list users';
    const ADD = 'add user';
    const GET = 'get user';
    const EDIT = 'edit user';
    const DELETE = 'delete user';
    const RESTORE = 'restore user';
    const MANAGE_PERMISSIONS = 'manage user permissions';

    /**
     * @inheritDoc
     */
    public static function toArray(): array
    {
        return [
            self::LIST,
            self::ADD,
            self::GET,
            self::EDIT,
            self::DELETE,
            self::RESTORE
        ];
    }
}
