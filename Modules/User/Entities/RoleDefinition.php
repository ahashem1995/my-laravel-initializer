<?php


namespace Modules\User\Entities;


use App\Entities\BaseFields;

class RoleDefinition
{
    const TABLE_NAME = 'roles';

    const ID = BaseFields::ID;
    const NAME = 'name';
}
