<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$routePrefix = Config::get('user.modulePrefix');

Route::middleware('auth:api')->prefix($routePrefix)->group(function () {
    Route::apiResource('roles', 'RoleController');
    Route::put('roles/{role}/syncUsers', 'RoleController@syncUsers')->name('roles.syncUsers');

    Route::get('users/search', 'UserController@search');
    Route::apiResource('users', 'UserController');
    Route::put('users/{user}/syncPermissions', 'UserController@syncPermissions')->name('users.syncPermissions');

    Route::apiResource('permissions', 'PermissionController')->only(['index']);
});
