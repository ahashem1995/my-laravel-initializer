<?php

namespace Modules\User\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\User\Console\MigratePermissions;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('User', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
		$this->app->singleton(\Modules\User\Services\PermissionService::class, \Modules\User\Services\PermissionService::class);

		$this->app->singleton(\Modules\User\Repositories\PermissionRepository::class, function () {
            return new \Modules\User\Repositories\PermissionRepository(Config::get('user.permissionPath'), Config::get('user.permissionDefaultOrder'));
        });
		$this->app->singleton(\Modules\User\Services\RoleService::class, \Modules\User\Services\RoleService::class);

		$this->app->singleton(\Modules\User\Repositories\RoleRepository::class, function () {
            return new \Modules\User\Repositories\RoleRepository(Config::get('user.rolePath'), Config::get('user.roleDefaultOrder'));
        });
		$this->app->singleton(\Modules\User\Services\UserService::class, \Modules\User\Services\UserService::class);

		$this->app->singleton(\Modules\User\Repositories\UserRepository::class, function () {
            return new \Modules\User\Repositories\UserRepository(Config::get('user.userPath'), Config::get('user.userDefaultOrder'));
        });
        $this->app->register(RouteServiceProvider::class);

        $this->commands([
            MigratePermissions::class,
        ]);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('User', 'Config/config.php') => config_path('user.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('User', 'Config/config.php'), 'user'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/user');

        $sourcePath = module_path('User', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/user';
        }, \Config::get('view.paths')), [$sourcePath]), 'user');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/user');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'user');
        } else {
            $this->loadTranslationsFrom(module_path('User', 'Resources/lang'), 'user');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('User', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
