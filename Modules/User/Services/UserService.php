<?php

namespace Modules\User\Services;

use App\Services\BaseService;

use App\User;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use Modules\User\Entities\UserDefinition;
use Modules\User\Repositories\UserRepository;

class UserService extends BaseService
{
	public function __construct(UserRepository $repository) {
		parent::__construct($repository);
	}

    /**
     * @param string $email
     * @param string $name
     * @return Model|null
     * @throws Exception
     */
    public function addUser(string $email, string $name)
    {
        return $this->create([
            UserDefinition::EMAIL => $email,
            UserDefinition::NAME => $name,
            UserDefinition::PASSWORD => '123456'
        ]);
    }

    public function editUser($user, string $email, string $name)
    {
        if (!($user instanceof User))
            $user = $this->find($user);

        if ($user === null)
            throw new ModelNotFoundException();

        $user->fill([
            UserDefinition::NAME => $name,
            UserDefinition::EMAIL => $email
        ]);

        $this->save($user);

        return $user;
    }
}
