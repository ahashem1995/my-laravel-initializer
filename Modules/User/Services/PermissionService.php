<?php

namespace Modules\User\Services;

use App\Services\BaseService;

use Modules\User\Repositories\PermissionRepository;

class PermissionService extends BaseService
{
	public function __construct(PermissionRepository $repository) {
		parent::__construct($repository);
	}
}
