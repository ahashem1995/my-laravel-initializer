<?php

namespace Modules\User\Services;

use App\Services\BaseService;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\User\Entities\RoleDefinition;
use Modules\User\Repositories\RoleRepository;
use Spatie\Permission\Models\Role;

class RoleService extends BaseService
{
	public function __construct(RoleRepository $repository) {
		parent::__construct($repository);
	}

    /**
     * @param string $name
     * @param array $permissionIds
     * @return Model|null
     * @throws \Exception
     */
    public function addRole(string $name, array $permissionIds)
    {
        $role = $this->create([
            RoleDefinition::NAME => $name
        ]);

        $this->save($role);

        $this->syncRelation('permissions', $role, $permissionIds);

        return $role;
    }

    /**
     * @param Role|int $role
     * @param string $name
     * @param array $permissionIds
     * @return Model|null
     * @throws \Exception
     */
    public function editRole($role, string $name, array $permissionIds)
    {
        if (!($role instanceof Role))
            $role = $this->find($role);

        if ($role === null)
            throw new ModelNotFoundException();

        $role->fill([
            RoleDefinition::NAME => $name,
        ]);

        $this->save($role);

        $this->syncRelation('permissions', $role, $permissionIds);

        return $role;
    }
}
