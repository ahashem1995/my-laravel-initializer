<?php

namespace Modules\User\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Modules\User\Repositories\PermissionRepository;
use PhpParser\Node\Stmt\Catch_;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MigratePermissions extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'migrate:permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate model permissions from code to DB.';

    /**
     * @var PermissionRepository $permissionsRepository
     */
    private $permissionsRepository;

    /**
     * Create a new command instance.
     *
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(PermissionRepository $permissionRepository)
    {
        parent::__construct();
        $this->permissionsRepository = $permissionRepository;
    }

    /**
     * Execute the console command.
     *
     * @return bool
     * @throws \Exception
     */
    public function handle()
    {
        $modules = array_diff(scandir('Modules'), ['.', '..']);

        foreach ($modules as $module)
        {
            $entityFiles = array_diff(scandir('Modules/' . $module . '/Entities'), ['.', '..']);

            foreach ($entityFiles as $entityFile)
            {
                if (Str::endsWith($entityFile, 'Permissions.php'))
                {
                    $permissionsClass = explode('.', 'Modules\\' . $module . '\\Entities\\' . $entityFile)[0];

                    foreach ($permissionsClass::toArray() as $permissionName)
                        try
                        {
                            $this->permissionsRepository->create(['name' => $permissionName, 'guard_name' => 'api']);
                        } catch (\Exception $e)
                        {
                            $this->error($e->getMessage());
                        }
                }
            }
        }

        return true;
    }
}
