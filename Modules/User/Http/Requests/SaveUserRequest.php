<?php

namespace Modules\User\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\User\Entities\UserDefinition;

class SaveUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'string'],
            'email' => ['required', 'email']
        ];

        if ($this->method() === 'PUT' || $this->method() === 'PATCH')
            $rules['email'][] = Rule::unique(UserDefinition::TABLE_NAME, UserDefinition::EMAIL)->ignore($this->route('user'));
        else
            $rules['email'][] = Rule::unique(UserDefinition::TABLE_NAME, UserDefinition::EMAIL);

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('update', $this->route('user'));
    }
}
