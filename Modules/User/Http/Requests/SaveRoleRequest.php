<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\User\Entities\PermissionDefinition;
use Modules\User\Entities\RoleDefinition;

class SaveRoleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required',],
            'permissions' => ['present', 'array'],
            'permissions.*' => [
                'required',
                'distinct',
                Rule::exists(PermissionDefinition::TABLE_NAME, PermissionDefinition::ID)
            ]
        ];

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['name'][] = Rule::unique(RoleDefinition::TABLE_NAME, RoleDefinition::NAME)->ignore($this->route('role'));
        }
        else
            $rules['name'][] = Rule::unique(RoleDefinition::TABLE_NAME, RoleDefinition::NAME);

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->method() === 'PUT' || $this->method() === 'PATCH')
            return auth()->user()->can('update', $this->route('role'));
        return auth()->user()->can('create', Role::class);
    }
}
