<?php

namespace Modules\User\Http\Requests;

use App\Http\Requests\BaseFormRequest;

class SearchUserRequest extends BaseFormRequest
{
    protected $queryParametersToValidate = ['query'];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query' => 'required|min:2'
        ];
    }
}
