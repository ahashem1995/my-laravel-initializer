<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\User\Entities\PermissionDefinition;
use Modules\User\Entities\RoleDefinition;

class SyncUserPermissionsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'roles' => [
                'present',
                'array'
            ],
            'roles.*' => [
                'required',
                'distinct',
                Rule::exists(RoleDefinition::TABLE_NAME, RoleDefinition::ID)
            ],
            'permissions' => [
                'present',
                'array'
            ],
            'permissions.*' => [
                'required',
                'distinct',
                Rule::exists(PermissionDefinition::TABLE_NAME, PermissionDefinition::ID)
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
