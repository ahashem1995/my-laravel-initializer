<?php

namespace Modules\User\Http\Requests;

use App\Http\Requests\ListPaginatedRequest;
use App\User;
use Modules\User\Entities\UserDefinition;

class ListUserRequest extends ListPaginatedRequest
{
    public function isSortable()
    {
        return true;
    }

    public function getSortables()
    {
        return array_keys(UserDefinition::SORTABLES);
    }

    public function authorize()
    {
        return auth()->user()->can('viewAny', User::class);
    }

}

