<?php

namespace Modules\User\Http\Controllers;

use App\Http\Controllers\MyVehicleBaseController;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Services\PermissionService;
use Modules\User\Transformers\PermissionResource;
use Spatie\Permission\Models\Permission;

class PermissionController extends MyVehicleBaseController
{
    /**
     * @var PermissionService
     */
    private $service;

    public function __construct(PermissionService $permissionService)
    {
        $this->service = $permissionService;
    }

    /**
     * Display a listing of the resource.
     * @return AnonymousResourceCollection
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Permission::class);

        $permissions = $this->service->all();

        return PermissionResource::collection($permissions);
    }

}
