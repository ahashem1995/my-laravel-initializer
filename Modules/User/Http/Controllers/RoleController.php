<?php

namespace Modules\User\Http\Controllers;

use App\Http\Controllers\MyVehicleBaseController;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\User\Entities\RoleDefinition;
use Modules\User\Http\Requests\SyncRoleUsersRequest;
use Modules\User\Http\Requests\SaveRoleRequest;
use Modules\User\Services\RoleService;
use Modules\User\Services\UserService;
use Modules\User\Transformers\RoleHeaderResource;
use Modules\User\Transformers\RoleResource;
use Spatie\Permission\Models\Role;

class RoleController extends MyVehicleBaseController
{

    /**
     * @var RoleService
     */
    private $service;

    private $userService;

    public function __construct(RoleService $roleService, UserService $userService)
    {
        $this->service = $roleService;
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Role::class);

        $roles = $this->service->all();

        return $this->ok(RoleHeaderResource::collection($roles));
    }

    /**
     * Store a newly created resource in storage.
     * @param SaveRoleRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function store(SaveRoleRequest $request)
    {
        $persistedRole = $this->service->addRole($request->get('name'), $request->get('permissions'));

        return $this->ok(new RoleResource($persistedRole));
    }

    /**
     * Show the specified resource.
     * @param Role $role
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function show(Role $role)
    {
        $this->authorize('view', $role);

        return $this->ok(new RoleResource($role));
    }

    /**
     * Update the specified resource in storage.
     * @param SaveRoleRequest $request
     * @param Role $role
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(SaveRoleRequest $request, Role $role)
    {
        try
        {
            $persistedRole = $this->service->editRole($role, $request->get('name'), $request->get('permissions'));
        }
        catch (ModelNotFoundException $e)
        {
            return $this->notFound();
        }

        return $this->ok(new RoleResource($persistedRole));
    }

    /**
     * Remove the specified resource from storage.
     * @param Role $role
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws \Exception
     */
    public function destroy(Role $role)
    {
        $this->authorize('delete', $role);

        $this->service->forceDeleteObject($role);

        return $this->deleted();
    }

    /**
     * @param SyncRoleUsersRequest $request
     * @param Role $role
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function syncUsers(SyncRoleUsersRequest $request, Role $role)
    {
        $userIds = $request->get('users');

        foreach ($userIds as $userId)
            $this->authorize('managePermissions', $this->userService->find($userId));

        $this->service->syncRelation('users', $role, $userIds);

        return $this->ok(new RoleResource($role));
    }
}
