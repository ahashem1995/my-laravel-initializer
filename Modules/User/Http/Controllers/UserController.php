<?php

namespace Modules\User\Http\Controllers;

use App\Http\Controllers\MyVehicleBaseController;
use App\Transformers\PaginationResource;
use App\User;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Modules\User\Entities\UserDefinition;
use Modules\User\Http\Requests\ListUserRequest;
use Modules\User\Http\Requests\SaveUserRequest;
use Modules\User\Http\Requests\SearchUserRequest;
use Modules\User\Http\Requests\SyncUserPermissionsRequest;
use Modules\User\Services\UserService;
use Modules\User\Transformers\UserHeaderResource;
use Modules\User\Transformers\UserResource;

class UserController extends MyVehicleBaseController
{
    /**
     * @var UserService
     */
    private $service;

    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Display a listing of the resource.
     * @param SearchUserRequest $request
     * @return PaginationResource
     * @throws AuthorizationException
     */
    public function search(SearchUserRequest $request)
    {
        $this->authorize('viewAny', User::class);

        $query = $request->get('query');

        $conditions = [[UserDefinition::NAME, 'like', '%' . $query . '%']];

        $users = $this->service->where($conditions);
        return new PaginationResource(UserHeaderResource::class, $users);
    }

    /**
     * Display a listing of the resource.
     * @param ListUserRequest $request
     * @return PaginationResource
     * @throws AuthorizationException
     */
    public function index(ListUserRequest $request)
    {
        $this->authorize('viewAny', User::class);

        $query = $request->get('query');
        $pageSize = $request->get('page_size', Config::get('user.defaultPageSize'));
        $orderBy = $request->get('order_by', null);
        $orderDir = $request->get('order_dir', null);

        if (empty($orderBy))
            $orderBy = UserDefinition::ID;
        else
            $orderBy = UserDefinition::SORTABLES[$orderBy];

        if (empty($orderDir))
            $orderDir = 'asc';

        $conditions = [];
        if ($query != null)
            $conditions[] = [UserDefinition::NAME, 'like', '%' . $query . '%'];

        $users = $this->service->paginateWith([], $pageSize, [$orderBy => $orderDir], $conditions);
        return new PaginationResource(UserHeaderResource::class, $users);
    }

    /**
     * Store a newly created resource in storage.
     * @param SaveUserRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function store(SaveUserRequest $request)
    {
        $persistedUser = $this->service->addUser($request->get('email'), $request->get('name'));

        return $this->ok(new UserResource($persistedUser));
    }

    /**
     * Show the specified resource.
     * @param User $user
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function show(User $user)
    {
        $this->authorize('view', $user);

        return $this->ok(new UserResource($user));
    }

    /**
     * Update the specified resource in storage.
     * @param SaveUserRequest $request
     * @param User $user
     * @return JsonResponse
     */
    public function update(SaveUserRequest $request, User $user)
    {
        try
        {
            $persistedUser = $this->service->editUser($user, $request->get('email'), $request->get('name'));
        }
        catch (ModelNotFoundException $e)
        {
            return $this->notFound();
        }

        return $this->ok(new UserResource($persistedUser));
    }

    /**
     * Remove the specified resource from storage.
     * @param User $user
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws Exception
     */
    public function destroy(User $user)
    {
        $this->authorize('delete', $user);

        $this->service->softDeleteObject($user);

        return $this->deleted();
    }

    /**
     * @param SyncUserPermissionsRequest $request
     * @param User $user
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function syncPermissions(SyncUserPermissionsRequest $request, User $user)
    {
        $roleIds = $request->get('roles');
        $permissionIds = $request->get('permissions');

        $this->authorize('managePermissions', $user);

        $this->service->syncRelation('roles', $user, $roleIds);
        $this->service->syncRelation('permissions', $user, $permissionIds);

        return $this->ok(new UserResource($user));
    }
}
