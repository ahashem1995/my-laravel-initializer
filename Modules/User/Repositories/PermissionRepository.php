<?php

namespace Modules\User\Repositories;

use App\Repositories\BaseRepository;

class PermissionRepository extends BaseRepository
{
	public function __construct($persistentClass, $defaultOrder = ['id' => 'desc']) {
		parent::__construct($persistentClass, $defaultOrder);
	}
}
