<?php

namespace Modules\User\Transformers;

use Illuminate\Http\Request;
use Modules\User\Entities\PermissionDefinition;

class UserIdentityResource extends UserHeaderResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     * @return array
     */
    public function toArray($request)
    {
        $array = parent::toArray($request);
        $array['permissions'] = $this->resource->getAllPermissions()->pluck(PermissionDefinition::NAME);

        return $array;
    }
}
