<?php

namespace Modules\User\Transformers;

use Illuminate\Http\Request;

class RoleResource extends RoleHeaderResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     * @return array
     */
    public function toArray($request)
    {
        $array = parent::toArray($request);
        $array['permissions'] = PermissionResource::collection($this->permissions);
        $array['users'] = UserHeaderResource::collection($this->users);

        return $array;
    }
}
