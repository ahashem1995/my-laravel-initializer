<?php

namespace Modules\User\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\User\Entities\UserDefinition;

class UserHeaderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this[UserDefinition::ID],
            'email' => $this[UserDefinition::EMAIL],
            'name' => $this[UserDefinition::NAME]
        ];
    }
}
