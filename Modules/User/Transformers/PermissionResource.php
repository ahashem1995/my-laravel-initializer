<?php

namespace Modules\User\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use Modules\User\Entities\PermissionDefinition;

class PermissionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this[PermissionDefinition::ID],
            'name' => $this[PermissionDefinition::NAME]
        ];
    }
}
