<?php

namespace Modules\User\Transformers;

use Illuminate\Http\Request;

class UserResource extends UserHeaderResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     * @return array
     */
    public function toArray($request)
    {
        $array = parent::toArray($request);
        $array['roles'] = RoleHeaderResource::collection($this->roles);
        $array['permissions'] = PermissionResource::collection($this->permissions);

        return $array;
    }
}
