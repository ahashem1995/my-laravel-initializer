<?php

namespace Modules\User\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\User\Entities\RolePermissions;
use Spatie\Permission\Models\Role;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo(RolePermissions::LIST);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo(RolePermissions::ADD);
    }

    /**
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function view(User $user, Role $role)
    {
        return $user->hasPermissionTo(RolePermissions::GET);
    }

    /**
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function update(User $user, Role $role)
    {
        return $user->hasPermissionTo(RolePermissions::EDIT);
    }

    /**
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function delete(User $user, Role $role)
    {
        return $user->hasPermissionTo(RolePermissions::DELETE);
    }

    /**
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function restore(User $user, Role $role)
    {
        return $user->hasPermissionTo(RolePermissions::RESTORE);
    }
}
