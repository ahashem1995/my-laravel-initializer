<?php

namespace Modules\User\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\User\Entities\PermissionPermissions;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo(PermissionPermissions::LIST);
    }
}
