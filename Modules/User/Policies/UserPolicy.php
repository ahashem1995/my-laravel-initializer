<?php

namespace Modules\User\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\User\Entities\UserPermissions;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo(UserPermissions::LIST);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo(UserPermissions::ADD);
    }

    /**
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function view(User $user, User $targetUser)
    {
        return $user->hasPermissionTo(UserPermissions::GET);
    }

    /**
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function update(User $user, User $targetUser)
    {
        return $user->hasPermissionTo(UserPermissions::EDIT);
    }

    /**
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function delete(User $user, User $targetUser)
    {
        return $user->hasPermissionTo(UserPermissions::DELETE);
    }

    /**
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function restore(User $user, User $targetUser)
    {
        return $user->hasPermissionTo(UserPermissions::RESTORE);
    }

    /**
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function managePermissions(User $user, User $targetUser)
    {
        return $user->hasPermissionTo(UserPermissions::MANAGE_PERMISSIONS);
    }
}
