<?php

namespace Modules\Country\Services;

use App\Services\BaseService;

use App\Services\Traits\SearchableServiceTrait;
use \Modules\Country\Repositories\CityRepository;

class CityService extends BaseService
{
    use SearchableServiceTrait;

    public function __construct(CityRepository $repository)
    {
        parent::__construct($repository);
    }
}
