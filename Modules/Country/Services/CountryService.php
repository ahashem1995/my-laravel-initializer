<?php

namespace Modules\Country\Services;

use App\Services\BaseService;

use Modules\Country\Repositories\CountryRepository;

class CountryService extends BaseService
{
	public function __construct(CountryRepository $repository) {
		parent::__construct($repository);
	}
}