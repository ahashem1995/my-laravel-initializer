<?php

namespace Modules\Country\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Country\Services\CountryService;
use function foo\func;

class CountryServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('Country', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
		$this->app->singleton(\Modules\Country\Services\CityService::class, \Modules\Country\Services\CityService::class);

		$this->app->singleton(\Modules\Country\Repositories\CityRepository::class, function () {
            return new \Modules\Country\Repositories\CityRepository(Config::get('country.cityPath'), Config::get('country.cityDefaultOrder'));
        });
		$this->app->singleton(\Modules\Country\Services\CountryService::class, \Modules\Country\Services\CountryService::class);

		$this->app->singleton(\Modules\Country\Repositories\CountryRepository::class, function () {
            return new \Modules\Country\Repositories\CountryRepository(Config::get('country.countryPath'),
                Config::get('country.countryDefaultOrder'));
        });


        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('Country', 'Config/config.php') => config_path('country.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('Country', 'Config/config.php'), 'country'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/country');

        $sourcePath = module_path('Country', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/country';
        }, \Config::get('view.paths')), [$sourcePath]), 'country');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/country');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'country');
        } else {
            $this->loadTranslationsFrom(module_path('Country', 'Resources/lang'), 'country');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('Country', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
