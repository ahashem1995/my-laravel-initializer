<?php


namespace Modules\Country\Entities;


use App\Entities\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;


class Country extends BaseModel
{
    use SoftDeletes;

    protected $table = CountryDefinition::TABLE_NAME;

    protected $fillable = CountryDefinition::FILLABLE_FIELDS;

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
