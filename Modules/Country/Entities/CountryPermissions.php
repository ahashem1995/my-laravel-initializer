<?php


namespace Modules\Country\Entities;


use App\Entities\BasePermissions;

class CountryPermissions extends BasePermissions
{
    const LIST = 'list countries';
    const ADD = 'add country';
    const GET = 'get country';
    const EDIT = 'edit country';
    const DELETE = 'delete country';
    const RESTORE = 'restore country';

    /**
     * @inheritDoc
     */
    public static function toArray(): array
    {
        return [
            self::LIST,
            self::ADD,
            self::GET,
            self::EDIT,
            self::DELETE,
            self::RESTORE
        ];
    }
}
