<?php


namespace Modules\Country\Entities;


use App\Entities\BaseFields;

class CountryDefinition
{
    const TABLE_NAME = "countries";

    const ID = BaseFields::ID;
    const NAME = "name";
    const ISO_ALPHA2_CODE = "iso_alpha2_code";
    const ISO_ALPHA3_CODE = "iso_alpha3_code";
    const INTERNATIONAL_PHONE_CODE = "code";
    const NATIONALITY = "nationality";
    const CONTINENT = "continent";

    const FILLABLE_FIELDS = [
        self::NAME,
        self::INTERNATIONAL_PHONE_CODE,
        self::ISO_ALPHA3_CODE,
        self::ISO_ALPHA2_CODE,
        self::NATIONALITY,
        self::CONTINENT
    ];

    const SORTABLES = [
        'id' => self::ID,
        'name' => self::NAME,
        'iso_alpha_3_code' => self::ISO_ALPHA3_CODE,
        'iso_alpha_2_code' => self::ISO_ALPHA2_CODE,
    ];
}
