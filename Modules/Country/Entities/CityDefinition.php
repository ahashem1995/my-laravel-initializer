<?php


namespace Modules\Country\Entities;


use App\Entities\BaseFields;

class CityDefinition
{
    const TABLE_NAME = "cities";

    const ID = BaseFields::ID;
    const COUNTRY_ID = 'country_id';
    const NAME = 'name';

    const FILLABLES = [
        self::COUNTRY_ID,
        self::NAME
    ];
}
