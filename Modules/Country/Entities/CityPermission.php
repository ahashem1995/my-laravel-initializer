<?php


namespace Modules\Country\Entities;


use App\Entities\BasePermissions;

class CityPermission extends BasePermissions
{
    const LIST = 'list cities';

    /**
     * @inheritDoc
     */
    public static function toArray(): array
    {
        return [
            self::LIST
        ];
    }
}
