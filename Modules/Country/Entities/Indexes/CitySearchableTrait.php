<?php


namespace Modules\Country\Entities\Indexes;


trait CitySearchableTrait
{
    protected $indexConfigurator = CityIndexConfigurator::class;
    protected $searchRules = [
        CitySearchRule::class
    ];

    protected $mapping = [
        'properties' => [
            'id' => [
                'type' => 'integer'
            ],
            'name' => [
                'type' => 'text',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                    ]
                ],
                'analyzer' => 'autocomplete',
                'search_analyzer' => 'autocomplete_search'
            ],
            'country' => [
                'type' => 'nested',
                'properties' => [
                    'id' => [
                        'type' => 'integer'
                    ],
                    'name' => [
                        'type' => 'text',
                        'analyzer' => 'autocomplete',
                        'search_analyzer' => 'autocomplete_search'
                    ]
                ]
            ]
        ]
    ];
}
