<?php

namespace Modules\Country\Entities\Indexes;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class CityIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = "cities";

    /**
     * @var array
     */
    protected $settings = [
        'analysis' => [
            'analyzer' => [
                'autocomplete' => [
                    'filter' => ['lowercase'],
                    'tokenizer' => 'autocomplete'
                ],
                'autocomplete_search' => [
                    'tokenizer' => 'lowercase'
                ]
            ],
            'tokenizer' => [
                'autocomplete' => [
                    'token_chars' => ['letter'],
                    'min_gram' => 2,
                    'max_gram' => 70,
                    'type' => 'edge_ngram'
                ]
            ]
        ]
    ];
}
