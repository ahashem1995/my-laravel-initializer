<?php


namespace Modules\Country\Entities\Indexes;


use ScoutElastic\SearchRule;

class CitySearchRule extends SearchRule
{
    public function buildQueryPayload()
    {
        $query = $this->builder->query;
        $querySegments = explode(' ', $query);
        $firstTerm = $querySegments[0];

        return [
            "should" => [
                [
                    "query_string" => [
                        "query" => $query,
                        "boost" => 3
                    ]
                ],
                [
                    'fuzzy' => [
                        'name' => [
                            'value' => $firstTerm
                        ]
                    ]
                ],
                [
                    "nested" => [
                        "path" => "country",
                        "query" => [
                            "query_string" => [
                                "query" => $query,
                                "boost" => 2
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
