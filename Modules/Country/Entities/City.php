<?php


namespace Modules\Country\Entities;


use App\Entities\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Country\Entities\Indexes\CitySearchableTrait;
use ScoutElastic\Searchable;

class City extends BaseModel
{
    use SoftDeletes, Searchable, CitySearchableTrait;

    protected $table = CityDefinition::TABLE_NAME;
    protected $fillable = CityDefinition::FILLABLES;

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function toSearchableArray()
    {
        return [
            'id' => $this[CityDefinition::ID],
            'name' => $this[CityDefinition::NAME],
            'country' => [
                'id' => $this->country[CountryDefinition::ID],
                'name' => $this->country[CountryDefinition::NAME],
            ]
        ];
    }
}
