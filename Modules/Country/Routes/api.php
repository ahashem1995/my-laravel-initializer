<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$routePrefix = Config::get('country.modulePrefix');

Route::middleware('auth:api')->prefix($routePrefix)->group(function () {
    Route::apiResource('countries', 'CountryController');
    Route::get('/cities', 'CityController@search')->name('cities.search');
});

