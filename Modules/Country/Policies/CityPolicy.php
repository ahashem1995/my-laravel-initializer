<?php

namespace Modules\Country\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Country\Entities\CityPermission;

class CityPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo(CityPermission::LIST);
    }
}
