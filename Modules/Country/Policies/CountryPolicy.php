<?php

namespace Modules\Country\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Country\Entities\Country;
use Modules\Country\Entities\CountryPermissions;

class CountryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo(CountryPermissions::LIST);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo(CountryPermissions::ADD);
    }

    /**
     * @param User $user
     * @param Country $country
     * @return bool
     */
    public function view(User $user, Country $country)
    {
        return $user->hasPermissionTo(CountryPermissions::GET);
    }

    /**
     * @param User $user
     * @param Country $country
     * @return bool
     */
    public function update(User $user, Country $country)
    {
        return $user->hasPermissionTo(CountryPermissions::EDIT);
    }

    /**
     * @param User $user
     * @param Country $country
     * @return bool
     */
    public function delete(User $user, Country $country)
    {
        return $user->hasPermissionTo(CountryPermissions::DELETE);
    }

    /**
     * @param User $user
     * @param Country $country
     * @return bool
     */
    public function restore(User $user, Country $country)
    {
        return $user->hasPermissionTo(CountryPermissions::RESTORE);
    }
}
