<?php

namespace Modules\Country\Repositories;

use App\Repositories\BaseRepository;
use App\Repositories\Traits\SearchableRepositoryTrait;

class CityRepository extends BaseRepository
{
    use SearchableRepositoryTrait;

	public function __construct($persistentClass, $defaultOrder = ['id' => 'desc']) {
		parent::__construct($persistentClass, $defaultOrder);
	}
}
