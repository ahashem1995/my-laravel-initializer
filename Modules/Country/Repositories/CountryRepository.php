<?php

namespace Modules\Country\Repositories;

use App\Repositories\BaseRepository;

class CountryRepository extends BaseRepository
{
	public function __construct($persistentClass, $defaultOrder = ['id' => 'desc']) {
		parent::__construct($persistentClass, $defaultOrder);
	}
}