<?php

namespace Modules\Country\Database\Seeders;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Modules\Country\Entities\City;
use Modules\Country\Entities\CityDefinition;
use Modules\Country\Entities\Country;
use Modules\Country\Entities\CountryDefinition;
use Modules\Country\Repositories\CityRepository;
use Modules\Country\Repositories\CountryRepository;

class CountryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param CountryRepository $countryRepository
     * @param CityRepository $cityRepository
     * @return void
     * @throws \Exception
     */
    public function run(CountryRepository $countryRepository, CityRepository $cityRepository)
    {
        Model::unguard();
        try {
            $jsonFile = Storage::disk('country')->get('countries.json');
            $data = json_decode($jsonFile);

            $persisted_countries = [];
            foreach ($data as $country) {
                //TODO: Add Continent
                $added = Country::doWithoutEvents(function () use ($countryRepository, $country) {
                    return $countryRepository->create([
                        CountryDefinition::NAME => $country->country,
                        CountryDefinition::INTERNATIONAL_PHONE_CODE => $country->country_code,
                        CountryDefinition::ISO_ALPHA2_CODE => $country->iso_alpha2_code,
                        CountryDefinition::ISO_ALPHA3_CODE => $country->iso_alpha3_code,
                        CountryDefinition::NATIONALITY => $country->nationality,
                        CountryDefinition::CONTINENT => "N/A"
                    ]);
                });

                $persisted_countries[$added[CountryDefinition::ISO_ALPHA3_CODE]] = $added[CountryDefinition::ID];
            }

            $jsonFile = Storage::disk('country')->get('cities.json');
            $cities_data = json_decode($jsonFile);
            foreach ($cities_data as $city) {
                try {
                    City::doWithoutEvents(function () use ($cityRepository, $persisted_countries, $city) {
                        $cityRepository->create([
                            CityDefinition::COUNTRY_ID => $persisted_countries[$city->country_code],
                            CityDefinition::NAME => $city->city
                        ]);
                    });
                } catch (\Exception $e) {
                    report($e);
                    //DO NOTHING
                }
            }
        } catch (FileNotFoundException $e) {
            report($e);
        }
    }
}
