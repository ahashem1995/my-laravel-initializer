<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Country\Entities\CountryDefinition;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CountryDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->increments(CountryDefinition::ID);
            $table->string(CountryDefinition::NAME);
            $table->string(CountryDefinition::NATIONALITY);
            $table->string(CountryDefinition::CONTINENT);
            $table->string(CountryDefinition::ISO_ALPHA2_CODE);
            $table->string(CountryDefinition::ISO_ALPHA3_CODE);
            $table->string(CountryDefinition::INTERNATIONAL_PHONE_CODE);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CountryDefinition::TABLE_NAME);
    }
}
