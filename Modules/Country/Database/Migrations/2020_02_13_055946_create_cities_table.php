<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Modules\Country\Entities\CityDefinition;
use \Modules\Country\Entities\CountryDefinition;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CityDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->increments(CityDefinition::ID);
            $table->unsignedInteger(CityDefinition::COUNTRY_ID);
            $table->string(CityDefinition::NAME);

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table(CityDefinition::TABLE_NAME, function (Blueprint $table) {
            $table->foreign(CityDefinition::COUNTRY_ID)
                ->references(CountryDefinition::ID)
                ->on(CountryDefinition::TABLE_NAME);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CityDefinition::TABLE_NAME);
    }
}
