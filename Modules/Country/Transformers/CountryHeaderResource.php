<?php

namespace Modules\Country\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Country\Entities\CountryDefinition;

class CountryHeaderResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this[CountryDefinition::ID],
            'name' => $this[CountryDefinition::NAME],
            'international_phone_code' => $this[CountryDefinition::INTERNATIONAL_PHONE_CODE],
            'iso_alpha_2_code' => $this[CountryDefinition::ISO_ALPHA2_CODE],
            'iso_alpha_3_code' => $this[CountryDefinition::ISO_ALPHA3_CODE],
        ];
    }
}
