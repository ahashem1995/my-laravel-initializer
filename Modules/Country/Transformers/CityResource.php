<?php

namespace Modules\Country\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Country\Entities\CityDefinition;

class CityResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this[CityDefinition::ID],
            'name' => $this[CityDefinition::NAME],
            'country' => new CountryHeaderResource($this->country)
        ];
    }
}
