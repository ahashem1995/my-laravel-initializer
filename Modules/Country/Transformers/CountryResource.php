<?php

namespace Modules\Country\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class CountryResource extends CountryHeaderResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $returnValue = parent::toArray($request);
        $returnValue['cities'] = CityNestedResource::collection($this->cities);
        return $returnValue;
    }
}
