<?php

namespace Modules\Country\Http\Requests;

use App\Http\Requests\BaseFormRequest;
use Modules\Country\Entities\City;

class SearchCityRequest extends BaseFormRequest
{
    protected $queryParametersToValidate = ['query'];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query' => 'required|min:1'
        ];
    }

    public function authorize()
    {
        return auth()->user->can('viewAny', City::class);
    }

}
