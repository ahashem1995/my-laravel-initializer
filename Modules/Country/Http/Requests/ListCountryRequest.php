<?php

namespace Modules\Country\Http\Requests;

use App\Http\Requests\ListPaginatedRequest;
use Modules\Country\Entities\Country;
use Modules\Country\Entities\CountryDefinition;

class ListCountryRequest extends ListPaginatedRequest
{
    public function isSortable()
    {
        return true;
    }

    public function getSortables()
    {
        return array_keys(CountryDefinition::SORTABLES);
    }

    public function authorize()
    {
        return auth()->user()->can('viewAny', Country::class);
    }
}
