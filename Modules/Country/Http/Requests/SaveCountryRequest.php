<?php

namespace Modules\Country\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\Country\Entities\Country;
use Modules\Country\Entities\CountryDefinition;

class SaveCountryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => [
                'required',
            ],
            'international_phone_code' => 'required|min:1',
            'iso_alpha2_code' => 'required|min:1',
            'iso_alpha3_code' => 'required|min:1',
            'nationality' => 'required',
        ];

        if ($this->method() == 'PUT' || $this->method() == 'PATCH') {
            $rules['name'][] = Rule::unique(CountryDefinition::TABLE_NAME, CountryDefinition::NAME)->ignore($this->route('country'));
        } else {
            $rules['name'][] = Rule::unique(CountryDefinition::TABLE_NAME, CountryDefinition::NAME);
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->method() === 'PUT' || $this->method() === 'PATCH')
            return auth()->user()->can('update', $this->route('country'));
        return auth()->user()->can('create', Country::class);
    }
}
