<?php

namespace Modules\Country\Http\Controllers;

use App\Http\Controllers\MyVehicleBaseController;
use App\Transformers\PaginationResource;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Modules\Country\Entities\Country;
use Modules\Country\Entities\CountryDefinition;
use Modules\Country\Http\Requests\ListCountryRequest;
use Modules\Country\Http\Requests\SaveCountryRequest;
use Modules\Country\Services\CountryService;
use Modules\Country\Transformers\CountryHeaderResource;
use Modules\Country\Transformers\CountryResource;

class CountryController extends MyVehicleBaseController
{
    private $service;

    public function __construct(CountryService $service)
    {
        $this->service = $service;
    }

    /**
     * @param ListCountryRequest $request
     * @return PaginationResource
     */
    public function index(ListCountryRequest $request)
    {
        $query = $request->get('query');
        $pageSize = $request->get('page_size', Config::get('country.defaultPageSize'));
        $orderBy = $request->get('order_by', null);
        $orderDir = $request->get('order_dir', null);

        if (empty($orderBy))
            $orderBy = CountryDefinition::NAME;
        else
            $orderBy = CountryDefinition::SORTABLES[$orderBy];

        if (empty($orderDir))
            $orderDir = 'asc';

        $conditions = [];
        if (!empty($query))
            $conditions[] = [CountryDefinition::NAME, 'like', '%' . $query . '%'];

        $countries = $this->service->paginate($pageSize, [$orderBy => $orderDir], $conditions);
        return new PaginationResource(CountryHeaderResource::class, $countries);
    }

    /**
     * Store a newly created resource in storage.
     * @param SaveCountryRequest $request
     * @return JsonResponse
     */
    public function store(SaveCountryRequest $request)
    {
        $created = $this->service->create([
            CountryDefinition::NAME => $request->get('name'),
            CountryDefinition::INTERNATIONAL_PHONE_CODE => $request->get('international_phone_code'),
            CountryDefinition::ISO_ALPHA2_CODE => $request->get('iso_alpha2_code'),
            CountryDefinition::ISO_ALPHA3_CODE => $request->get('iso_alpha3_code'),
            CountryDefinition::NATIONALITY => $request->get('nationality'),
            CountryDefinition::CONTINENT => $request->get('continent', 'N/A')
        ]);

        return $this->ok(new CountryHeaderResource($created));
    }

    /**
     * Show the specified resource.
     * @param Country $country
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function show(Country $country)
    {
        $this->authorize('view', $country);

        return $this->ok(new CountryResource($country));
    }

    /**
     * Update the specified resource in storage.
     * @param SaveCountryRequest $request
     * @param Country $country
     * @return JsonResponse
     */
    public function update(SaveCountryRequest $request, Country $country)
    {
        $country->fill([
            CountryDefinition::NAME => $request->get('name'),
            CountryDefinition::INTERNATIONAL_PHONE_CODE => $request->get('international_phone_code'),
            CountryDefinition::ISO_ALPHA2_CODE => $request->get('iso_alpha2_code'),
            CountryDefinition::ISO_ALPHA3_CODE => $request->get('iso_alpha3_code'),
            CountryDefinition::NATIONALITY => $request->get('nationality'),
            CountryDefinition::CONTINENT => $request->get('continent', 'N/A')
        ]);

        $this->service->save($country);

        return $this->ok(new CountryHeaderResource($country));
    }

    /**
     * Remove the specified resource from storage.
     * @param Country $country
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws \Exception
     */
    public function destroy(Country $country)
    {
        $this->authorize('delete', $country);

        $this->service->softDeleteObject($country);

        return $this->deleted();
    }
}
