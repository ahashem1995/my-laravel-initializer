<?php

namespace Modules\Country\Http\Controllers;

use App\Http\Controllers\MyVehicleBaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Country\Http\Requests\SearchCityRequest;
use Modules\Country\Services\CityService;
use Modules\Country\Transformers\CityResource;

class CityController extends MyVehicleBaseController
{
    private $service;

    public function __construct(CityService $service)
    {
        $this->service = $service;
    }

    public function search(SearchCityRequest $request)
    {
        $cities = $this->service->search($request->get('query'));

        return $this->ok(CityResource::collection($cities));
    }
}
