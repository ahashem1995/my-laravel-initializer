<?php

return [
    'permissionPath' => 'Spatie\Permission\Models\Permission',
    'permissionOrderColumn' => false,
    'permissionDefaultOrder' => ['id' => 'desc'],
    'rolePath' => 'Spatie\Permission\Models\Role',
    'roleOrderColumn' => false,
    'roleDefaultOrder' => ['id' => 'desc'],
    'userPath' => 'App\User',
    'userOrderColumn' => false,
    'userDefaultOrder' => ['id' => 'desc'],
    'name' => 'User',
    'modulePrefix' => 'user',
    'defaultPageSize' => 15,
];
