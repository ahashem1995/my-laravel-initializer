<?php

return [
	'attachmentPath' => 'Modules\Common\Entities\Attachment',
	'attachmentOrderColumn' => false,
	'attachmentDefaultOrder' => ['id' => 'desc'],
	'notePath' => 'Modules\Common\Entities\Note',
	'noteOrderColumn' => false,
	'noteDefaultOrder' => ['id' => 'desc'],
    'name' => 'Common',
    'modulePrefix' => 'common',
    'googleMapsApiKey' => env('GOOGLE_MAPS_API_KEY'),
    'flightStatsAppId' => env('FLIGHT_STATS_APP_ID'),
    'flightStatsAppKey' => env('FLIGHT_STATS_APP_KEY')
];
