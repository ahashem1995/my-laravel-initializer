<?php

return [
	'itemPath' => 'Modules\Service\Entities\Service',
	'itemOrderColumn' => false,
	'itemDefaultOrder' => ['id' => 'desc'],
	'servicetypeitemPath' => 'Modules\Service\Entities\ServiceCategory',
	'servicetypeitemOrderColumn' => false,
	'servicetypeitemDefaultOrder' => ['id' => 'desc'],
	'servicetypePath' => 'Modules\Service\Entities\Category',
	'servicetypeOrderColumn' => false,
	'servicetypeDefaultOrder' => ['id' => 'desc'],
    'servicePath' => 'Modules\Service\Entities\AirportVendor',
    'serviceOrderColumn' => false,
    'serviceDefaultOrder' => ['id' => 'desc'],
    'modulePrefix' => 'services',
    'name' => 'Service',
    'defaultPageSize' => 15,
];
