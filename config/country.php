<?php

return [
    'cityPath' => '\Modules\Country\Entities\City',
    'cityOrderColumn' => false,
    'cityDefaultOrder' => ['id' => 'desc'],
    'countryPath' => 'Modules\Country\Entities\Country',
    'countryOrderColumn' => false,
    'countryDefaultOrder' => ['id' => 'desc'],
    'name' => 'Country',
    'modulePrefix' => 'geo',
    'defaultPageSize' => 15
];
