<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use Tymon\JWTAuth\JWTAuth;

class ForceUserLogout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($id = auth()->id()) {
            $forceLogoutKey = 'force-user-' . $id . '-logout';
            if (Cache::has($forceLogoutKey)) {
                Cache::forget($forceLogoutKey);
                auth()->invalidate();
                // Terminate continuing request
                return response()->json(['message' => 'Unauthorized'], 401);
            }
        }

        return $next($request);
    }
}
