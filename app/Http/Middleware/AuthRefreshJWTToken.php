<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\JWTAuth;

class AuthRefreshJWTToken
{
    protected $except_urls = [
        'auth/login',
    ];

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $regex = '#' . implode('|', $this->except_urls) . '#';

        if (preg_match($regex, $request->path()) || !Str::startsWith($request->path(), '/api'))
        {
            return $next($request);
        }

        try {
            if (!auth()->check()) {
                return response()->json([
                    'message' => 'unauthenticated',
                    'data' => []
                ], 401);
            }
        } catch (TokenExpiredException $e) {
            // If the token is expired, then it will be refreshed and added to the headers
            try {
                $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                $user = JWTAuth::setToken($refreshed)->toUser();
                header('token' . $refreshed);
            } catch (JWTException $e) {
                return response()->json([
                    'message' => 'unauthenticated',
                    'data' => []
                ], 401);
            }
        } catch (JWTException $e) {
            return response()->json([
                'message' => 'unauthenticated',
                'data' => []
            ], 401);
        }

        return $next($request);
    }
}
