<?php

namespace App\Http\Middleware;

use Closure;

class VerifyJWTExpiration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($response->status() !== 401) {
            // 21600 seconds = 6 hours
            if (auth()->user() && (time() > auth()->payload()['iat'] + 21600)) {
                $encodedNewContent = json_encode(
                    array_merge(
                        ['data' => json_decode($response->getContent(), true)],
                        ['refresh_token' => auth()->refresh()]
                    ));
                $response->setContent($encodedNewContent);
            }
        }

        return $response;
    }

}
