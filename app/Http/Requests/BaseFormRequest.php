<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

abstract class BaseFormRequest extends FormRequest
{
    protected $routeParametersToValidate = [];
    protected $queryParametersToValidate = [];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules();

    public function all($keys = null)
    {
        $data = parent::all();

        foreach ($this->routeParametersToValidate as $routeParameter) {
            $data[$routeParameter] = $this->route($routeParameter);
        }

        foreach ($this->queryParametersToValidate as $queryParameter) {
            $data[$queryParameter] = $this->query($queryParameter);
        }

        return $data;
    }
}
