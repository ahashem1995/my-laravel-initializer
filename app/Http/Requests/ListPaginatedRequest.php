<?php


namespace App\Http\Requests;


use Illuminate\Validation\Rule;

abstract class ListPaginatedRequest extends BaseFormRequest
{
    protected $queryParametersToValidate = [
        'query',
        'page',
        'page_size',
        'order_by',
        'order_dir'
    ];

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [
            'query' => 'nullable|string',
            'page' => 'nullable|integer|min:1',
            'page_size' => 'nullable|integer|min:1'
        ];

        if ($this->isSortable()) {
            $rules['order_by'] = [
                'nullable',
                Rule::in($this->getSortables())
            ];

            $rules['order_dir'] = [
                'nullable',
                Rule::in(['asc', 'desc'])
            ];
        }

        return $rules;
    }

    public function isSortable()
    {
        return false;
    }

    public function getSortables()
    {
        return [];
    }
}
