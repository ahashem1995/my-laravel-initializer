<?php


namespace App\Entities\Messaging;


use JsonSerializable;

class Response //implements JsonSerializable
{
    public $data;
    public $message;

    public function __construct($data = null, $message = null)
    {
        $this->data = $data;
        $this->message = $message;
    }

//    /**
//     * @inheritDoc
//     */
//    public function jsonSerialize()
//    {
//        $returnValue = [];
//
//        if ($this->data != null)
//            $returnValue['data'] = $this->data;
//
//        if ($this->message != null)
//            $returnValue['message'] = $this->message;
//    }
}
