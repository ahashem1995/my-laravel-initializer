<?php


namespace App\Entities;


use App\Events\ModelCreated;
use App\Events\ModelDeleted;
use App\Events\ModelUpdated;
use App\Events\SpecificModelCreated;
use App\Events\SpecificModelDeleted;
use App\Events\SpecificModelUpdated;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class BaseModel extends Model
{
    use Cachable;

    protected $hidden = [
        BaseFields::UPDATED_AT,
        BaseFields::CREATED_AT
    ];

    /**
     * Get Current Class Name
     * @return string
     */
    public static function getClass()
    {
        return get_called_class();
    }

    /**
     * Get Parent Class Name
     * @return string
     */
    public static function getDefiningClass()
    {
        return get_class();
    }

    /**
     *  Get Table Name
     * @return string
     */
    public static function getTableName()
    {
        return (new static)->getTable();
    }

    /**
     * Get Module Prefix
     * @return string
     */
    public static function getModule()
    {
        $moduleName = explode('\\', self::getClass())[1];
        return Config::get(Str::lower($moduleName) . '.modulePrefix');
    }

    /**
     * Get Singular Name Form
     * @return string
     */
    public static function getSingular()
    {
        return Str::camel(class_basename(self::getClass()));
    }

    /**
     * Get Plural Name Form
     * @return string
     */
    public static function getPlural()
    {
        return Str::plural(self::getSingular());
    }

    protected static function boot()
    {
        parent::boot();
        self::created(function ($model) {
            broadcast(new ModelCreated($model))->toOthers();
        });

        self::updated(function ($model) {
            broadcast(new ModelUpdated($model))->toOthers();
            broadcast(new SpecificModelUpdated($model))->toOthers();
        });

        self::deleted(function ($model) {
            broadcast(new ModelDeleted($model))->toOthers();
            broadcast(new SpecificModelDeleted($model))->toOthers();
        });
    }

    public static function doWithoutEvents(callable $callback)
    {
        return static::withoutEvents($callback);
    }
}
