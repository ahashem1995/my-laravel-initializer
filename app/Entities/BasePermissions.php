<?php


namespace App\Entities;


abstract class BasePermissions
{
    /**
     * @return array
     */
    abstract public static function toArray(): array;

}
