<?php


namespace App\Repositories\Traits;


trait SearchableRepositoryTrait
{
    /**
     * @param string $query
     * @param array $projection Select Fields
     * @param array $conditions Conditions
     * @param int $skip Skip number of documents
     * @param int $take Limit of documents returned
     * @return array|null
     * @throws \Exception
     */
    public function search(string $query, array $projection = [], array $conditions = [], int $skip = 0, $take = 15)
    {
        try {
            $query_set = $this->persistentClass::search($query);

            if (count($projection) > 0)
                $query_set->select($projection);

            if (count($conditions) > 0)
                if (array_keys($conditions) !== range(0, count($conditions) - 1))
                    foreach ($conditions as $field => $value)
                        $query_set->where($field, $value);
                else
                    foreach ($conditions as $condition)
                        $query_set->where($condition[0], $condition[1], $condition[2]);

            $query_set->from($skip);
            $query_set->take($take);
            return $query_set->get();
        } catch (\Exception $e) {
            report($e);
            throw $e;
        }
    }
}
