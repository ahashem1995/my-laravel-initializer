<?php


namespace App\Repositories\Contracts;


use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    /**
     * @param array $order
     * @param array $columns
     * @return mixed
     */
    public function all($order = array(), $columns = array('*'));


    /**
     * @param int $perPage
     * @param array $order
     * @param array $conditions
     * @param bool $onlyTrashed
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 15, $order = array(), $conditions = array(), $onlyTrashed = false, $columns = array('*'));

    /**
     * @param array $tables
     * @param int $perPage
     * @param array $order
     * @param array $conditions
     * @param bool $onlyTrashed
     * @param array $columns
     * @return mixed
     */
    public function paginateWith($tables = array(), $perPage = 15, $order = array(), $conditions = array(), $onlyTrashed = false, $columns = array('*'));

    /**
     * @param array $conditions
     * @param array $order
     * @param array $columns
     * @return mixed
     */
    public function where($conditions = array(), $order = array(), $columns = array('*'));

    /**
     * @param string $field
     * @param array $items
     * @param array $order
     * @param array $columns
     * @return mixed
     * @throws \Exception
     */
    public function whereIn(string $field, array $items = array(), $order = array(), $columns = array('*'));

    /**
     * @param array $tables
     * @param array $conditions
     * @param array $columns
     * @return mixed
     */
    public function with($tables = array(), $conditions = array(), $columns = array('*'));

    /**
     * @param array $order
     * @param array $columns
     * @return mixed array of soft deleted data
     */
    public function getTrashed($order = array(), $conditions = array(), $columns = array('*'));

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'));

    /**
     * @param $field
     * @param $value
     * @param array $order
     * @param array $columns
     * @return mixed
     */
    public function findByProperty($field, $value, $order = array(), $columns = array('*'));

    /**
     * @param Model $model
     * @return mixed
     */
    public function save(Model $model);

    /**
     * @param array $data
     * @param $attributeValues
     * @param string $attribute
     * @return bool|mixed
     */
    public function update(array $data, $attributeValues, $attribute = 'id');

    /**
     * @param array $data
     * @return Model
     */
    public function create(array $data): ?Model;

    /**
     * @param array|int $ids
     * @return bool
     */
    public function softDelete($ids): bool;

    /**
     * @param $conditions
     * @return bool
     */
    public function softDeleteWhere($conditions): bool;

    /**
     * @param $object
     * @return bool
     * @throws \Exception
     */
    public function softDeleteObject($object): bool;

    /**
     * @param array|int $ids
     * @return bool
     */
    public function forceDelete($ids): bool;

    /**
     * @param $conditions
     * @return bool
     */
    public function forceDeleteWhere($conditions): bool;

    /**
     * @param $object
     * @return bool
     * @throws \Exception
     */
    public function forceDeleteObject($object): bool;

    /**
     * @param $ids
     * @return bool|mixed
     */
    public function restore($ids);

    /**
     * @param $field
     * @param $value
     * @return mixed
     */
    public function has($field, $value);

    /**
     * Deletes all data in a table
     * @return mixed
     */
    public function truncate();

    /**
     * @param $relation
     * @param $model
     * @param array $related
     * @return mixed
     */
    public function syncRelation($relation, $model, array $related);
}
