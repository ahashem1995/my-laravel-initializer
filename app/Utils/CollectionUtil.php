<?php


namespace App\Utils;

class CollectionUtil
{
    static function get(&$var, $default = null)
    {
        return isset($var) ? $var : $default;
    }
}

