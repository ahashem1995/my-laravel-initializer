<?php


namespace App\Utils;



class ConversionUtil
{
    /**
     * @param $value
     * @return false|float|string
     */
    private static function coordinatesDecimalToDegrees($value)
    {
        $newVal = floor($value);
        $newVal .= '-';
        $newVal .= ($value - floor($value)) * 60;

        return $newVal;
    }

    /**
     * @param float $lat
     * @param float $lng
     * @return string
     *
     * Convert from WGS84 to GPS coordinates format.
     */
    public static function wgs84ToGPS(float $lat, float $lng): string
    {
        $newLat = $lat >= 0 ? 'n' : 's';
        $newLat .= static::coordinatesDecimalToDegrees(abs($lat));

        $newLng = $lng >= 0 ? 'e' : 'w';
        $newLng .= static::coordinatesDecimalToDegrees(abs($lng));

        return $newLat . '/' . $newLng;
    }

    /**
     * @param $value
     * @return float
     */
    private static function coordinatesDegreesToDecimal($value)
    {
        $unaryOperator = $value[0] === 'n' || $value[0] === 'e' ? '+' : '-';

        $value = substr($value, 1);

        [$degrees, $minutes] = explode('-', $value);

        $decimals = (float) $minutes / 60;

        return (float) ($unaryOperator . ((float) $degrees + $decimals));
    }

    /**
     * @param string $coordinates
     * @return array
     *
     * Convert from GPS to WGS84 coordinates format.
     */
    public static function gpsToWGS84(string $coordinates)
    {
        [$lat, $lng] = explode('/', $coordinates);

        return [static::coordinatesDegreesToDecimal($lat), static::coordinatesDegreesToDecimal($lng)];
    }
}
