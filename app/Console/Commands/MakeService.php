<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {--name=} {--module=} {--repository=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new service for specified module & repository';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->validate()) {
            return false;
        }

        $serviceName = $this->option('name');
        if ($serviceName == null) {
            $repositoryNameSegments = explode('/', $this->option('repository'));
            $repositoryName = $repositoryNameSegments[count($repositoryNameSegments) - 1];
            $serviceName = str_replace('Repository', '', $repositoryName) . 'Service';
        }

        if (!$this->createService($serviceName)) {
            return false;
        }

        $this->registerInProviders($serviceName);
        return true;
    }

    private function createService($serviceName)
    {
        $modulePath = module_path($this->option('module'));
        $repositoriesPath = $modulePath . DIRECTORY_SEPARATOR . 'Services';
        $filePath = $repositoriesPath . DIRECTORY_SEPARATOR . $serviceName . '.php';

        if (!file_exists($repositoriesPath)) {
            mkdir($repositoriesPath, 0777, true);
        }
        if (file_exists($filePath)) {
            $this->error("Service '" . $filePath . "' already exists");
            return false;
        }
        file_put_contents($filePath, $this->repositoryTemplate($serviceName));
        return true;
    }

    private function registerInProviders($serviceName)
    {
        $modulePath = module_path($this->option('module'));

        $providerFilePath = $modulePath . DIRECTORY_SEPARATOR . 'Providers' . DIRECTORY_SEPARATOR . $this->option('module') . 'ServiceProvider.php';

        $serviceName = '\Modules\\' . $this->option('module') . '\Services\\' . $serviceName;

        $newContent = "";
        $word = "";
        $handle = fopen($providerFilePath, "r+");
        while (!feof($handle)) {
            $char = fgetc($handle);
            $newContent .= $char;
            if (($char <= 'z' && $char >= 'a') ||
                ($char <= 'Z' && $char >= 'A') ||
                ($char <= '9' && $char >= '0') ||
                $char === ')' ||
                $char === '(') {
                $word .= $char;
            } else {
                $word = "";
            }
            if ($word === "register()") {
                while (($t = fgetc($handle)) != "{") {
                    $newContent .= $t;
                }
                $newContent .= $t;

                while (($t = fgetc($handle)) != "\n") {
                    $newContent .= $t;
                }
                $newContent .= $t;
                break;
            }
        }
        $newContent .= $this->singletonTemplate($serviceName);
        while (!feof($handle)) {
            $newContent .= fgetc($handle);
        }
        fclose($handle);
        file_put_contents($providerFilePath, $newContent);
    }

    private function singletonTemplate($serviceName)
    {
        $className = $serviceName . '::class';
        return "\t\t" . '$this->app->singleton(' . $className . ', ' . $className . ');' . "\n\n";
    }

    private function repositoryTemplate($serviceName)
    {
        $repositorySegments = explode('/', $this->option('repository'));

        //TODO: Inject Primary Repository
        $template = "<?php\n\n";
        $template .= 'namespace Modules\\' . $this->option('module') . '\\Services;' . "\n\n";
        $template .= 'use App\Services\BaseService;' . "\n\n";
        $template .= 'use ' . str_replace('/', '\\', $this->option('repository')) . ';' . "\n\n";
        $template .= 'class ' . $serviceName . ' extends BaseService' . "\n";
        $template .= "{\n";
        $template .= "\t" . 'public function __construct(' . $repositorySegments[count($repositorySegments) - 1] . ' $repository) {' . "\n";
        $template .= "\t\t" . 'parent::__construct($repository);' . "\n";
        $template .= "\t" . '}' . "\n";
        $template .= '}';
        return $template;
    }

    private function validate()
    {
        if (!$this->option('repository')) {
            $this->error('Repository option (--repository=path to model) is missing');
            return false;
        }
        if (!$this->option('module')) {
            $this->error('Module name option (--module=module name) is missing');
            return false;
        }

        return true;
    }
}
