<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeRepository extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {--name=} {--module=} {--model=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository for specified module & model';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->validate()) {
            return false;
        }

        $modelName = $this->option('name');
        if($modelName == null) {
            $modelFullPath = explode('/', $this->option('model'));
            $modelName = $modelFullPath[count($modelFullPath) - 1];
        }

        if (!$this->createRepository($modelName)) {
            return false;
        }

        $this->addToConfig($modelName);
        $this->registerInProviders($modelName);
        return true;
    }

    private function createRepository($modelName)
    {
        $modulePath = module_path($this->option('module'));
        $repositoriesPath = $modulePath . DIRECTORY_SEPARATOR . 'Repositories';
        $filePath = $repositoriesPath . DIRECTORY_SEPARATOR . $modelName . 'Repository.php';

        if (!file_exists($repositoriesPath)) {
            mkdir($repositoriesPath, 0777, true);
        }
        if (file_exists($filePath)) {
            $this->error("Repository '" . $filePath . "' already exists");
            return false;
        }
        file_put_contents($filePath, $this->repositoryTemplate($modelName));
        return true;
    }

    private function addToConfig($modelName)
    {
        $modulePath = module_path($this->option('module'));

        $configFilePath = $modulePath . DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR . 'config.php';

        $newContent = "";
        $word = "";
        $handle = fopen($configFilePath, "r+");
        while (!feof($handle)) {
            $char = fgetc($handle);
            $newContent .= $char;
            if (($char <= 'z' && $char >= 'a') ||
                ($char <= 'Z' && $char >= 'A') ||
                ($char <= '9' && $char >= '0') ||
                $char === ')' ||
                $char === '(') {
                $word .= $char;
            } else {
                $word = "";
            }
            if ($word === "return") {
                while (($t = fgetc($handle)) != "[") {
                    $newContent .= $t;
                }
                $newContent .= $t;

                while (($t = fgetc($handle)) != "\n") {
                    $newContent .= $t;
                }
                $newContent .= $t;
                break;
            }
        }
        $newContent .= $this->configEntryTemplate(strtolower($modelName) . 'Path', "'" . str_replace('/', '\\', $this->option('model')) . "'");
        $newContent .= $this->configEntryTemplate(strtolower($modelName) . 'OrderColumn', 'false');
        $newContent .= $this->configEntryTemplate(strtolower($modelName) . 'DefaultOrder', "['id' => 'desc']");

        while (!feof($handle)) {
            $newContent .= fgetc($handle);
        }
        fclose($handle);
        file_put_contents($configFilePath, $newContent);
    }

    private function registerInProviders($modelName)
    {
        $modulePath = module_path($this->option('module'));

        $providerFilePath = $modulePath . DIRECTORY_SEPARATOR . 'Providers' . DIRECTORY_SEPARATOR . $this->option('module') . 'ServiceProvider.php';

        $repositoryName = '\Modules\\' . $this->option('module') . '\Repositories\\' . $modelName . 'Repository';

        $explodedModelName = explode('/', $this->option('model'));

        $newContent = "";
        $word = "";
        $handle = fopen($providerFilePath, "r+");
        while (!feof($handle)) {
            $char = fgetc($handle);
            $newContent .= $char;
            if (($char <= 'z' && $char >= 'a') ||
                ($char <= 'Z' && $char >= 'A') ||
                ($char <= '9' && $char >= '0') ||
                $char === ')' ||
                $char === '(') {
                $word .= $char;
            } else {
                $word = "";
            }
            if ($word === "register()") {
                while (($t = fgetc($handle)) != "{") {
                    $newContent .= $t;
                }
                $newContent .= $t;

                while (($t = fgetc($handle)) != "\n") {
                    $newContent .= $t;
                }
                $newContent .= $t;
                break;
            }
        }
        $newContent .= $this->singletonTemplate($repositoryName, $explodedModelName);
        while (!feof($handle)) {
            $newContent .= fgetc($handle);
        }
        fclose($handle);
        file_put_contents($providerFilePath, $newContent);
    }

    private function configEntryTemplate($key, $value)
    {
        return "\t'" . $key . "' => " . $value . ",\n";
    }

    private function singletonTemplate($repositoryName, $explodedModelName)
    {
        return "\t\t".'$this->app->singleton(' . $repositoryName . '::class, function () {
            return new ' . $repositoryName . "(Config::get('" . strtolower($this->option('module')) . '.' . strtolower($explodedModelName[count($explodedModelName) - 1]) . "Path'), Config::get('" . strtolower($this->option('module')) . "." . strtolower($explodedModelName[count($explodedModelName) - 1]) . "DefaultOrder'));
        });\n";
    }

    private function repositoryTemplate($modelName)
    {
        $template = "<?php\n\n";
        $template .= 'namespace Modules\\' . $this->option('module') . '\\Repositories;' . "\n\n";
        $template .= 'use App\Repositories\BaseRepository;' . "\n\n";
        $template .= 'class ' . $modelName. 'Repository extends BaseRepository' . "\n";
        $template .= "{\n";
        $template .= "\t" . 'public function __construct($persistentClass, $defaultOrder = [\'id\' => \'desc\']) {' . "\n";
        $template .= "\t\t" . 'parent::__construct($persistentClass, $defaultOrder);' . "\n";
        $template .= "\t" . '}' . "\n";
        $template .= '}';
        return $template;
    }

    private function validate()
    {
        if (!$this->option('model')) {
            $this->error('Model option (--model=path to model) is missing');
            return false;
        }
        if (!$this->option('module')) {
            $this->error('Module name option (--module=module name) is missing');
            return false;
        }
        return true;
    }
}
