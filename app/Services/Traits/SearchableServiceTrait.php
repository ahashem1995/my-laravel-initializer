<?php


namespace App\Services\Traits;


trait SearchableServiceTrait
{
    /**
     * @param string $query
     * @param array $projection Select Fields
     * @param array $conditions Conditions
     * @param int $skip Skip number of documents
     * @param int $take Limit of documents returned
     * @return array|null
     */
    public function search(string $query, array $projection = [], array $conditions = [], int $skip = 0, $take = 15)
    {
        return $this->primaryRepository->search($query, $projection, $conditions, $skip, $take);
    }
}
