<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Modules\Common\Database\Seeders\CommonDatabaseSeeder;
use Modules\Country\Database\Seeders\CountryDatabaseSeeder;
use Modules\Country\Repositories\CityRepository;
use Modules\Country\Repositories\CountryRepository;
use Modules\User\Repositories\RoleRepository;
use Modules\User\Repositories\UserRepository;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @param CountryRepository $countryRepository
     * @param CityRepository $cityRepository
     * @param UserRepository $userRepository
     * @param RoleRepository $roleRepository
     * @return void
     */
    public function run(UserRepository $userRepository,
                        RoleRepository $roleRepository,
                        CountryRepository $countryRepository,
                        CityRepository $cityRepository)
    {
        // Run Initial Data Seeds
        try {
            $versionCode = Config::get('app.app_version_number', 1);
            if ($versionCode <= 1) {
                Schema::disableForeignKeyConstraints();
                $cityRepository->truncate();
                $countryRepository->truncate();
                $roleRepository->truncate();
                $userRepository->truncate();
                Schema::enableForeignKeyConstraints();

                $this->call(CommonDatabaseSeeder::class);
                $this->call(CountryDatabaseSeeder::class);
            }

//            Config::set('app.app_version_code', 2);
        } catch (Exception $exception) {
            $this->command->error($exception->getMessage());
        }
    }
}
